package main

import (
	"github.com/AndroidStudyOpenSource/mpesa-api-go"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/julienschmidt/httprouter"
	"log"
	"mwangaza/app"
	"mwangaza/core/airtime"
	"mwangaza/core/business"
	kplc "mwangaza/core/power/api"
	"mwangaza/core/power/core"
	"mwangaza/core/shop"
	"mwangaza/core/shops"
	"mwangaza/core/tv"
	"mwangaza/core/users"
	"mwangaza/core/wallet"
	"mwangaza/core/water"
	"mwangaza/core/water/api"
	"mwangaza/core/whatsapp"
	"mwangaza/model"
	"mwangaza/settings"
	"net/http"
	"os"
)

// Database Connection variables
//var (
//	dbHost     = "/cloudsql/mwangaza-backend-223809:us-central1:mwangaza-production"
//	dbName     = "mwangaza"
//	dbUser     = "mwangaza"
//	dbPassword = "Ge1y6bLJ2LqFdECt"
//)

//var (
//	dbHost     = "localhost"
//	dbName     = "mwangaza"
//	dbUser     = "postgres"
//	dbPassword = "postgres"
//)

var (
	dbHost     = "ec2-52-54-182-29.compute-1.amazonaws.com"
	dbName     = "daagqlf6sar047"
	dbUser     = "uav58vfilfhnl0"
	dbPassword = "pefa50c415c4c32d07be22949f4fdd5e137ded242fba66bfa0a808cc803ce458c"
)

//postgresql://mwangaza:vydbfae3dslcaqod@db-postgresql-nyc1-16055-do-user-4474808-0.a.db.ondigitalocean.com:25060/mwangaza

func main() {

	// Initialize the Database here
	database, err := gorm.Open("postgres", "host="+dbHost+" user="+dbUser+" dbname="+dbName+" password="+dbPassword)

	// Only log on Dev and Staging Environments! Disable on Production
	if settings.GetEnv() == "PRODUCTION" {
		database.LogMode(true)
	} else {
		database.LogMode(true)
	}

	if err != nil {
		panic("Failed to connect to database")
	}

	defer database.Close()

	// Trigger Migrations on Database Connection
	runMigrations(database)

	//Initialize KPLC API
	power, _ := kplc.New("mwangaza", "C9p4j7twGxV2GzpYa7ET5j", kplc.PRODUCTION)
	waterAPI, _ := api.New("10631", "9uwdk4v42016$#", api.PRODUCTION)

	//Initialize MPESA API
	var mpesaB2C mpesa.Service
	mpesaB2C, err = mpesa.New("SFVZEASWM2RS3tWwRORI9rUiRLurmLmB", "byoGbagg0Sppj4bw", mpesa.PRODUCTION)

	// Create Router
	router := httprouter.New()
	modApp := app.New(database)
	modWhatsapp := whatsapp.New(database)
	modUser := users.New(database)
	modShop := shops.New(database)
	modBusiness := business.New(database)
	modShops := shop.New(database, power)
	modWallet := wallet.New(database, mpesaB2C)
	modWater := water.New(database, waterAPI)
	modAirtime := airtime.New(database, power)
	modTv := tv.New(database)
	modPower := core.New(database, power)

	// -----------------------------------------------------------------------------------------------------------------
	// MAIN API ENDPOINT
	// -----------------------------------------------------------------------------------------------------------------

	router.GET("/", modApp.CheckApiHealth)

	// -----------------------------------------------------------------------------------------------------------------
	// USER AUTH API's
	// -----------------------------------------------------------------------------------------------------------------

	router.POST("/api/v1/user/verify", modUser.VerifyUser)

	router.POST("/api/v1/user/register", modUser.RegisterUser)

	router.POST("/api/v1/user/login", modUser.LoginUser)

	// -----------------------------------------------------------------------------------------------------------------
	// SHOP AUTH API's
	// -----------------------------------------------------------------------------------------------------------------

	router.POST("/api/v1/shop/register", modShop.RegisterShop)

	router.POST("/api/v1/shop/login", modShop.LoginShop)

	// -----------------------------------------------------------------------------------------------------------------
	// VENDOR AUTH API's
	// -----------------------------------------------------------------------------------------------------------------

	router.POST("/api/v1/business/register", modBusiness.RegisterBusiness)

	router.POST("/api/v1/business/login", modBusiness.LoginBusiness)

	router.GET("/api/v1/list/business", modBusiness.GetBusiness)

	// -----------------------------------------------------------------------------------------------------------------
	// CONFIGURATION API's
	// -----------------------------------------------------------------------------------------------------------------

	router.POST("/api/v1/config", modApp.AppConfigs)

	router.GET("/api/v1/list/banks", modApp.GetBanks)

	router.POST("/api/v1/create/bank", modApp.CreateBank)

	router.GET("/api/v1/config/phoness", modApp.Last5PhoneNumbers)

	router.GET("/api/v1/config/prepaid/meters", modApp.Last5PrePaidMeterNumbers)

	router.GET("/api/v1/config/postpaid/meters", modApp.Last5PostPaidMeterNumbers)

	router.GET("/api/v1/config/water/meters", modApp.Last5PostPaidWaterNumbers)

	router.POST("/api/v1/user/fcm/token", modUser.SetFCMToken)

	router.POST("/api/v1/user/update/info", modUser.UpdateUserInfo)

	router.POST("/api/v1/user/pin/reset", modUser.ResetPin)

	router.POST("/api/v1/user/otp/verify", modUser.VerifyOTP)

	router.POST("/api/v1/user/pin/update", modUser.UpdatePIN)

	// -----------------------------------------------------------------------------------------------------------------
	// WALLET API's
	// -----------------------------------------------------------------------------------------------------------------

	router.GET("/api/v1/wallet/balance", modWallet.WalletBalance)

	router.GET("/api/v1/wallet/my/transactions", modWallet.Transactions)

	router.POST("/api/v1/wallet/topup", modWallet.UpdateTopUps)

	router.POST("/api/v1/wallet/pull", modWallet.UpdatePull)

	router.POST("/api/v1/wallet/reconcile", modWallet.ReconcileWallet)

	router.GET("/api/v1/wallet/deposits", modWallet.WalletDeposits)

	router.POST("/api/v1/mpesa/xpress", modWallet.XpressCallback)

	router.GET("/api/v1/wallet/transactions", modWallet.FetchTransactions)

	router.POST("/api/v1/wallet/send/money", modWallet.SendMoney)

	router.POST("/api/v1/b2c/result", modWallet.B2CResult)

	router.POST("/api/v1/b2c/timeout", modWallet.B2CTimeout)

	router.POST("/api/v1/wallet/pay/bill", modWallet.PayBill)

	router.POST("/api/v1/b2b/result", modWallet.B2BResult)

	router.POST("/api/v1/b2b/timeout", modWallet.B2BTimeout)

	router.POST("/api/v1/wallet/share/funds", modWallet.ShareFunds)

	// -----------------------------------------------------------------------------------------------------------------
	// AIRTIME API's
	// -----------------------------------------------------------------------------------------------------------------

	router.POST("/api/v1/airtime/buy", modAirtime.BuyAirtime)

	router.POST("/api/v1/voucher/user", modAirtime.BuyAirtimeVoucher)

	// -----------------------------------------------------------------------------------------------------------------
	// TV & INTERNET API's
	// -----------------------------------------------------------------------------------------------------------------

	router.POST("/api/v1/utilities/pay", modTv.PayUtilities)

	// -----------------------------------------------------------------------------------------------------------------
	// K.P.L.C PREPAID REQUEST API's
	// -----------------------------------------------------------------------------------------------------------------

	router.POST("/api/v1/power/prepaid/verify", modPower.VerifyPrepaid)

	router.POST("/api/v1/power/prepaid/purchase", modPower.PurchasePrepaid)

	router.GET("/api/v1/list/token/history", modPower.FetchRecentTokens)

	// -----------------------------------------------------------------------------------------------------------------
	// K.P.L.C POSTPAID REQUEST API's
	// -----------------------------------------------------------------------------------------------------------------

	router.POST("/api/v1/power/postpay/verify", modPower.VerifyPostpaid)

	router.POST("/api/v1/power/postpay/purchase", modPower.PurchasePostpaid)

	// -----------------------------------------------------------------------------------------------------------------
	// WATER API's
	// -----------------------------------------------------------------------------------------------------------------

	router.POST("/api/v1/water/nairobi/fetch", modWater.FetchNairobiWaterBill)

	router.POST("/api/v1/water/kisumu/fetch", modWater.FetchKisumuWaterBill)

	router.POST("/api/v1/water/bill/pay", modWater.PayWaterBill)

	// -----------------------------------------------------------------------------------------------------------------
	// WHATSAPP API's
	// -----------------------------------------------------------------------------------------------------------------

	router.POST("/api/v1/twilio/message", modWhatsapp.TwillioMessage)

	router.POST("/api/v1/twilio/callback", modWhatsapp.TwillioCallback)

	// -----------------------------------------------------------------------------------------------------------------
	// SHOP API's
	// -----------------------------------------------------------------------------------------------------------------

	router.POST("/api/v1/airtime/shop", modShops.BuyShopAirtime) // 6%

	router.POST("/api/v1/voucher/shop", modShops.BuyShopAirtimeVoucher) // 6%

	router.POST("/api/v1/shop/pay/bill", modWallet.ShopPayBill) // 30%

	router.POST("/api/v1/shop/utilities/pay", modTv.PayShopUtilities)

	router.POST("/api/v1/power/shop/prepaid/purchase", modPower.PurchasePrepaidShop) // 6%

	router.POST("/api/v1/power/shop/postpay/purchase", modPower.PurchasePostpaidShop)

	router.POST("/api/v1/shop/send/money", modWallet.SendMoneyShop) // New

	router.GET("/api/v1/shop/balance", modWallet.ShopBalance)

	router.GET("/api/v1/shop/transfer", modWallet.CommissionToBalance)

	//router.POST("/api/v1/shop/share/funds", modWallet.ShareFundsShop) // New

	// -----------------------------------------------------------------------------------------------------------------
	// START SERVER
	// -----------------------------------------------------------------------------------------------------------------

	log.Fatal(http.ListenAndServe(getPort(), router))

}

func getPort() string {
	port := os.Getenv("PORT")
	if settings.GetEnv() == "DEMO" {
		if port == "" {
			port = "8080"
			log.Println("no DEMO PORT environment variable detected. Setting port to ", port)
		}
	} else if settings.GetEnv() == "STAGING" {
		if port == "" {
			port = "8080"
			log.Println("no STAGING PORT environment variable detected. Setting port to ", port)
		}
	} else if settings.GetEnv() == "PRODUCTION" {
		if port == "" {
			port = "8080"
			log.Println("no PRODUCTION PORT environment variable detected. Setting port to ", port)
		}
	}
	return ":" + port
}

// running migrations here
func runMigrations(database *gorm.DB) {
	// Run all Migrations
	database.AutoMigrate(&model.User{}, &model.Wallet{}, &model.MpesaTransaction{}, &model.Water{}, &model.Airtime{},
		&model.PrePaid{}, &model.PostPaid{}, &model.MpesaB2cPayments{}, &model.MpesaB2bPayments{}, &model.Transaction{},
		&model.SendMoneyTariff{}, &model.PaybillTariff{}, &model.Bank{}, &model.Otp{}, &model.AirtimeVoucher{},
		&model.ShopAirtimeVoucher{}, &model.ShopWallet{}, &model.ShopAirtime{}, &model.ShopTransaction{},
		&model.Utility{}, &model.MpesaPull{}, &model.Business{}, &model.BusinessWallet{}, &model.MpesaReconcile{})
}
