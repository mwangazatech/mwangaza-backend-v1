package notification

import (
	"fmt"
	"github.com/AndroidStudyOpenSource/africastalking-go/sms"
	"github.com/douglasmakey/go-fcm"
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
	"log"
	"math/rand"
	"mwangaza/model"
	"time"
)

// Handler is a route routes for requests
type Handler struct {
	database *gorm.DB
}

// New creates a new Handler
func New(database *gorm.DB) Handler {
	return Handler{database: database}
}

var from = "Mwangaza"

func (h Handler) SendOTP(userId uuid.UUID, name, to string) {

	otp := otpRandSeq(6)
	message := "Hello " + name + ". Your OTP for Mwangaza is " + otp

	id := uuid.NewV4()
	h.database.Create(&model.Otp{OtpId: id, UserId: userId, Otp: otp, Active: true})

	// Create new SMS Service
	smsService := sms.NewService("mwangaza", "09edec383b376c278ee4d46630205ed6eae292c5adcf412f13c89a6a6ab99321", "Production")

	// Send SMS
	smsResponse, err := smsService.Send(from, to, message)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(smsResponse)

}

func init() {
	rand.Seed(time.Now().UnixNano())
}

var letters = []rune("123456789")

func otpRandSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func (h Handler) SendPushNotification(title, body, token string) {
	// init client
	client := fcm.NewClient("AAAAA9wSyhs:APA91bGxKg-Qnag5ERRnBN37-cK_QK8cBAfFySIW0kua03-GEelsJDIokylz2SgC9q85kOsLmRWlN5LMd5mF7J0Rh1mHVd1mD42RgBJt1Eig3jv_aoTd4iwuEfsVqZuibh4TKIgpteI9")

	// You can use your HTTPClient
	//client.SetHTTPClient(client)

	data := map[string]interface{}{
		"message": title,
		"details": map[string]string{
			"name":  "Name",
			"user":  "Admin",
			"thing": "none",
		},
	}

	client.PushSingle(token, data)

	// registrationIds remove and return map of invalid tokens
	badRegistrations := client.CleanRegistrationIds()
	log.Println(badRegistrations)

	status, err := client.Send()
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	log.Println(status.Results)
}
