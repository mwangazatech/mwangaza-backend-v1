package shops

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"github.com/julienschmidt/httprouter"
	"github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
	"mwangaza/model"
	"mwangaza/settings"
	"net/http"
	"strings"
)

// Handler is a route routes for requests
type Handler struct {
	database *gorm.DB
}

// New creates a new Handler
func New(database *gorm.DB) Handler {
	return Handler{database: database}
}

// ---------------------------------------------------------------------------------------------------------------------
// USER API's
// ---------------------------------------------------------------------------------------------------------------------

// RegisterUser This is responsible for registering a user
func (h Handler) RegisterShop(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var shop model.Shop
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&shop)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	id := uuid.NewV4()

	hash, err := HashPassword(shop.Pin)
	var phoneNumber string
	if strings.HasPrefix(shop.PhoneNumber, "07") {
		phoneNumber = "254" + shop.PhoneNumber[1:]
	} else {
		phoneNumber = shop.PhoneNumber
	}


	res := h.database.Create(&model.User{UserId: id, Name: shop.ShopName, PhoneNumber: phoneNumber,
		Pin: hash, FacebookToken: shop.FacebookToken, Shop: true}).Scan(&model.User{})

	resp := res.Value
	actual := resp.(*model.User)

	if actual.Mwangaza.Active == true {

		//Create Wallet with User ID
		h.createShopWallet(id)

		var userAccount = model.ShopAccount{}
		userAccount.ShopId = actual.UserId
		userAccount.ShopName = actual.Name
		userAccount.PhoneNumber = actual.PhoneNumber
		userAccount.DeviceId = actual.DeviceId
		userAccount.FacebookToken = actual.FacebookToken
		userAccount.Active = actual.Mwangaza.Active
		userAccount.JwtToken.Token = createToken(actual.UserId)

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(userAccount)

	} else {
		resp := new(model.Response)
		resp.Message = "Shop already registered"

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		_ = json.NewEncoder(w).Encode(resp)
	}

	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

}

func (h Handler) createShopWallet(userId uuid.UUID) {

	id := uuid.NewV4()
	_ = h.database.Create(&model.ShopWallet{WalletId: id, ShopId: userId, Balance: -499, Commission: 0}).Scan(&model.ShopWallet{})

}

// LoginUser This is responsible for logging in a user
func (h Handler) LoginShop(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var shop model.Shop
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&shop)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	var phoneNumber string
	if strings.HasPrefix(shop.PhoneNumber, "07") {
		phoneNumber = "254" + shop.PhoneNumber[1:]
	} else {
		phoneNumber = shop.PhoneNumber
	}


	res := h.database.Where(&model.User{PhoneNumber: phoneNumber}).First(&model.User{})

	resp := res.Value
	actual := resp.(*model.User)

	if len(actual.PhoneNumber) > 0 {
		if actual.Shop == true {

			match := CheckPasswordHash(shop.Pin, actual.Pin)

			if match == true {

				var userAccount = model.ShopAccount{}
				userAccount.ShopId = actual.UserId
				userAccount.ShopName = actual.Name
				userAccount.PhoneNumber = actual.PhoneNumber
				userAccount.DeviceId = actual.DeviceId
				userAccount.FacebookToken = actual.FacebookToken
				userAccount.Active = actual.Mwangaza.Active
				userAccount.JwtToken.Token = createToken(actual.UserId)

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				_ = json.NewEncoder(w).Encode(userAccount)

			} else {
				resp := new(model.Response)
				resp.Message = "Wrong Password"

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				_ = json.NewEncoder(w).Encode(resp)
			}
		} else {
			resp := new(model.Response)
			resp.Message = "Shop Not Found"

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusUnauthorized)
			_ = json.NewEncoder(w).Encode(resp)
		}

		if err != nil {
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
	}
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func createToken(userId uuid.UUID) string {

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"userId": userId,
	})

	tokenString, err := token.SignedString([]byte(settings.GetSecret()))
	if err != nil {
		fmt.Println(err)
	}

	return tokenString
}

func (h Handler) SetFCMToken(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				_ = json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				var firebase model.Firebase
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&firebase)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				claims, _ := token.Claims.(jwt.MapClaims)

				res := h.database.Model(&model.Shop{}).Where("shop_id =?", claims["userId"]).UpdateColumn("device_id", firebase.FirebaseToken)

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				_ = json.NewEncoder(w).Encode(res.Value)

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				_ = json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		_ = json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}
