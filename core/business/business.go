package business

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"github.com/julienschmidt/httprouter"
	"github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
	"mwangaza/model"
	"mwangaza/settings"
	"net/http"
	"strings"
)

// Handler is a route routes for requests
type Handler struct {
	database *gorm.DB
}

// New creates a new Handler
func New(database *gorm.DB) Handler {
	return Handler{database: database}
}

// ---------------------------------------------------------------------------------------------------------------------
// BUSINESS API's
// ---------------------------------------------------------------------------------------------------------------------

// RegisterUser This is responsible for registering a user
func (h Handler) RegisterBusiness(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var business model.Business
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&business)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	id := uuid.NewV4()

	hash, err := HashPassword(business.Pin)
	var phoneNumber string
	if strings.Contains(business.PhoneNumber, "+") {
		str := strings.Split(business.PhoneNumber, "+")
		phoneNumber = str[1]
	} else {
		phoneNumber = business.PhoneNumber
	}

	res := h.database.Create(&model.Business{BusinessId: id, FullNames: business.FullNames, BusinessAddress: business.BusinessAddress,
		BusinessLocation: business.BusinessLocation, BusinessDescription: business.BusinessDescription, PhoneNumber: phoneNumber,
		Pin: hash, ReferralCode: business.ReferralCode}).Scan(&model.Business{})

	resp := res.Value
	actual := resp.(*model.Business)

	if actual.Mwangaza.Active == true {

		//Create Wallet with User ID
		h.createBusinessWallet(id)

		var account = model.BusinessAccount{}
		account.BusinessId = actual.BusinessId
		account.FullNames = actual.FullNames
		account.BusinessAddress = actual.BusinessAddress
		account.BusinessLocation = actual.BusinessLocation
		account.BusinessDescription = actual.BusinessDescription
		account.PhoneNumber = actual.PhoneNumber
		account.ReferralCode = actual.ReferralCode
		account.Active = actual.Mwangaza.Active
		account.JwtToken.Token = createToken(actual.BusinessId)

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(account)

	} else {
		resp := new(model.Response)
		resp.Message = "Shop already registered"

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		_ = json.NewEncoder(w).Encode(resp)
	}

	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

}

func (h Handler) createBusinessWallet(userId uuid.UUID) {

	id := uuid.NewV4()
	_ = h.database.Create(&model.BusinessWallet{WalletId: id, BusinessId: userId, Balance: 0}).Scan(&model.BusinessWallet{})

}

// LoginUser This is responsible for logging in a user
func (h Handler) LoginBusiness(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var business model.Business
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&business)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	res := h.database.Where(&model.Business{PhoneNumber: business.PhoneNumber}).First(&model.Business{})

	resp := res.Value
	actual := resp.(*model.Business)

	if len(actual.PhoneNumber) > 0 {

		match := CheckPasswordHash(business.Pin, actual.Pin)

		if match == true {

			var account = model.BusinessAccount{}
			account.BusinessId = actual.BusinessId
			account.FullNames = actual.FullNames
			account.BusinessAddress = actual.BusinessAddress
			account.BusinessLocation = actual.BusinessLocation
			account.BusinessDescription = actual.BusinessDescription
			account.PhoneNumber = actual.PhoneNumber
			account.ReferralCode = actual.ReferralCode
			account.Active = actual.Mwangaza.Active
			account.JwtToken.Token = createToken(actual.BusinessId)

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			_ = json.NewEncoder(w).Encode(account)

		} else {
			resp := new(model.Response)
			resp.Message = "Wrong Password"

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusUnauthorized)
			_ = json.NewEncoder(w).Encode(resp)
		}

		if err != nil {
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
	} else {
		resp := new(model.Response)
		resp.Message = "Business Not Found"

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		_ = json.NewEncoder(w).Encode(resp)
	}
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func createToken(userId uuid.UUID) string {

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"userId": userId,
	})

	tokenString, err := token.SignedString([]byte(settings.GetSecret()))
	if err != nil {
		fmt.Println(err)
	}

	return tokenString
}

func (h Handler) GetBusiness(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				rows, err := h.database.Model(&model.Business{}).Rows()
				if err != nil {
					fmt.Println(err)
				}

				var business []model.Business
				for rows.Next() {
					var busines model.Business
					h.database.ScanRows(rows, &busines)
					business = append(business, busines)
				}

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				json.NewEncoder(w).Encode(business)

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}
}
