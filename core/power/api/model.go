package api

type Auth struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type AuthRes struct {
	Status  int    `json:"status"`
	Msg     string `json:"msg"`
	Balance int    `json:"balance"`
	Key     string `json:"key"`
}

type Balance struct {
	Username string `json:"username"`
	Key      string `json:"key"`
}

type PrePaidRequest struct {
	Username string `json:"username"`
	Key      string `json:"key"`
	Meter    string `json:"meter"`
}

type PostPaidRequest struct {
	Username  string `json:"username"`
	Key       string `json:"key"`
	AccountNo string `json:"accountNo"`
}

type VerifyPrePaid struct {
	Meter string `json:"meter"`
}

type VerifyPostPaid struct {
	AccountNo string `json:"accountNo"`
}

type PrePaidVendRequest struct {
	Username     string `json:"username"`
	Key          string `json:"key"`
	Meter        string `json:"meter"`
	Amount       string `json:"amount"`
	CustomerName string `json:"customerName"`
}

type PrePaidVend struct {
	CustomerName string `json:"customerName"`
	Meter        string `json:"meter"`
	Amount       string `json:"amount"`
}

type PostPaidVendRequest struct {
	Username     string `json:"username"`
	CustomerName string `json:"customerName"`
	Amount       string `json:"amount"`
	Key          string `json:"key"`
	AccountNo    string `json:"accountNo"`
}

type PostPaidVend struct {
	CustomerName string `json:"customerName"`
	AccountNo    string `json:"accountNo"`
	Amount       string `json:"amount"`
}

type PrePaidRes struct {
	Status       int    `json:"status"`
	Msg          string `json:"msg"`
	CustomerName string `json:"customerName"`
	Action       string `json:"action"`
}

type PostPaidRes struct {
	Status          int    `json:"status"`
	Msg             string `json:"msg"`
	CustomerName    string `json:"customerName"`
	CustomerBalance string `json:"customer_balance"`
	Action          string `json:"action"`
}

type PrePaidVendRes struct {
	Status      int    `json:"status"`
	Msg         string `json:"msg"`
	Transaction struct {
		Status         int           `json:"status"`
		Units          string        `json:"units"`
		Vendor         string        `json:"vendor"`
		MeterNo        string        `json:"meterNo"`
		Rescode        string        `json:"rescode"`
		StdTokenTax    string        `json:"stdTokenTax"`
		StdTokenAmt    string        `json:"stdTokenAmt"`
		Msg            string        `json:"msg"`
		Token          string        `json:"token"`
		StdTokenRctNum string        `json:"stdTokenRctNum"`
		Date           string        `json:"date"`
		TotalAmount    string        `json:"totalAmount"`
		TransactionID  int           `json:"transactionId"`
		Fixed          []interface{} `json:"fixed"`
		Ref            string        `json:"ref"`
		CustomerName   string        `json:"customerName"`
	} `json:"transaction"`
}

type PostPaidVendRes struct {
	Status      int    `json:"status"`
	Msg         string `json:"msg"`
	Transaction struct {
		Status        int    `json:"status"`
		RctNum        string `json:"rctNum"`
		Vendor        string `json:"vendor"`
		AccountNo     string `json:"accountNo"`
		Amount        string `json:"amount"`
		Date          string `json:"date"`
		TransactionID int    `json:"transactionId"`
		Ref           string `json:"ref"`
		CustomerName  string `json:"customerName"`
	} `json:"transaction"`
}

type Airtime struct {
	Username string `json:"username"`
	Operator string `json:"operator"`
	Amount   string `json:"amount"`
	Key      string `json:"key"`
	MobileNo string `json:"mobileno"`
}

type AirtimeReq struct {
	Operator string `json:"operator"`
	Amount   string `json:"amount"`
	MobileNo string `json:"mobileno"`
}

type AirtimeVoucher struct {
	Username string `json:"username"`
	Operator string `json:"operator"`
	Amount   string `json:"amount"`
	Key      string `json:"key"`
	Ref      string `json:"ref"`
}

type AirtimeVoucherReq struct {
	Operator string `json:"operator"`
	Amount   string `json:"amount"`
	Ref      string `json:"ref"`
}

type AirtimeRes struct {
	Status      int    `json:"status"`
	Msg         string `json:"msg"`
	Transaction struct {
		Status        int    `json:"status"`
		Vendor        string `json:"vendor"`
		Operator      string `json:"operator"`
		Receiptno     string `json:"receiptno"`
		Amount        string `json:"amount"`
		Date          string `json:"date"`
		TransactionID int    `json:"transactionId"`
		Type          string `json:"type"`
	} `json:"transaction"`
}

type AirtimeVoucherRes struct {
	Status      int    `json:"status"`
	Msg         string `json:"msg"`
	Transaction struct {
		Status        int    `json:"status"`
		Vendor        string `json:"vendor"`
		Pincode       string `json:"pincode"`
		Operator      string `json:"operator"`
		Amount        string `json:"amount"`
		Date          string `json:"date"`
		TransactionID int    `json:"transactionId"`
	} `json:"transaction"`
}
