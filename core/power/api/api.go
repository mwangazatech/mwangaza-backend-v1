package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// Env is the environment type
type Env string

const (
	// SANDBOX is the sandbox env tag
	SANDBOX = iota
	// PRODUCTION is the production env tag
	PRODUCTION
)

// Service is an TidalSpace Service
type Service struct {
	Username string
	Password string
	Env      int
}

// New return a new TidalSpace Service
func New(username, password string, env int) (Service, error) {
	return Service{username, password, env}, nil
}

//Generate TidalSpace Access Key
func (s Service) auth() (string, error) {

	auth := Auth{}
	auth.Username = s.Username
	auth.Password = s.Password

	body, err := json.Marshal(auth)
	reader := bytes.NewReader(body)
	if err != nil {
		return "", nil
	}

	url := s.baseURL() + "login"

	req, err := http.NewRequest(http.MethodPost, url, reader)
	if err != nil {
		return "", err
	}
	//Create an HTTP Client to set Request Headers
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("cache-control", "no-cache¬")
	client := &http.Client{Timeout: 10 * time.Second}
	res, err := client.Do(req)
	if res != nil {
		defer res.Body.Close()
	}
	if err != nil {
		return "", fmt.Errorf("could not send auth request: %v", err)
	}

	var authResponse AuthRes
	err = json.NewDecoder(res.Body).Decode(&authResponse)
	if err != nil {
		return "", fmt.Errorf("could not decode auth response: %v", err)
	}

	accessToken := authResponse.Key
	return accessToken, nil
}

// Balance checks the Balance
func (s Service) Balance() (string, error) {

	key, err := s.auth()
	if err != nil {
		return "", nil
	}

	balance := Balance{}
	balance.Username = s.Username
	balance.Key = key

	body, err := json.Marshal(balance)
	if err != nil {
		return "", nil
	}

	headers := make(map[string]string)
	headers["content-type"] = "application/json"
	headers["cache-control"] = "no-cache"

	url := s.baseURL() + "balance"
	return s.newReq(url, body, headers)
}

// VerifyMeter shows the customer details
func (s Service) VerifyPrePaidMeter(verifyReq VerifyPrePaid) (string, error) {

	key, err := s.auth()
	if err != nil {
		return "", nil
	}

	verify := PrePaidRequest{}
	verify.Username = s.Username
	verify.Key = key
	verify.Meter = verifyReq.Meter

	body, err := json.Marshal(verify)
	if err != nil {
		return "", nil
	}

	headers := make(map[string]string)
	headers["content-type"] = "application/json"
	headers["cache-control"] = "no-cache"

	url := s.baseURL() + "verify"
	return s.newReq(url, body, headers)
}

// Vend buys the Pre Paid Power
func (s Service) VendPrePaid(vendReq PrePaidVend) (string, error) {

	key, err := s.auth()
	if err != nil {
		return "", nil
	}

	vend := PrePaidVendRequest{}
	vend.Username = s.Username
	vend.Key = key
	vend.Meter = vendReq.Meter
	vend.Amount = vendReq.Amount
	vend.CustomerName = vendReq.CustomerName

	body, err := json.Marshal(vend)
	if err != nil {
		return "", nil
	}

	headers := make(map[string]string)
	headers["content-type"] = "application/json"
	headers["cache-control"] = "no-cache"

	url := s.baseURL() + "vend"
	return s.newReq(url, body, headers)
}

// VerifyMeter shows the customer details
func (s Service) VerifyPostPaidMeter(verifyReq VerifyPostPaid) (string, error) {

	key, err := s.auth()
	if err != nil {
		return "", nil
	}

	verify := PostPaidRequest{}
	verify.Username = s.Username
	verify.Key = key
	verify.AccountNo = verifyReq.AccountNo

	body, err := json.Marshal(verify)
	if err != nil {
		return "", nil
	}

	headers := make(map[string]string)
	headers["content-type"] = "application/json"
	headers["cache-control"] = "no-cache"

	url := s.baseURL() + "postpaidverify"
	return s.newReq(url, body, headers)
}

// Vend buys the Pre Paid Power
func (s Service) VendPostPaid(vendReq PostPaidVend) (string, error) {

	key, err := s.auth()
	if err != nil {
		return "", nil
	}

	vend := PostPaidVendRequest{}
	vend.Username = s.Username
	vend.Key = key
	vend.AccountNo = vendReq.AccountNo
	vend.Amount = vendReq.Amount
	vend.CustomerName = vendReq.CustomerName

	body, err := json.Marshal(vend)
	if err != nil {
		return "", nil
	}

	headers := make(map[string]string)
	headers["content-type"] = "application/json"
	headers["cache-control"] = "no-cache"

	url := s.baseURL() + "postpaidbill"
	return s.newReq(url, body, headers)
}

// BuyAirtime buys Pinless Airtime
func (s Service) BuyAirtime(airtimeReq AirtimeReq) (string, error) {

	key, err := s.auth()
	if err != nil {
		return "", nil
	}

	airtime := Airtime{}
	airtime.Username = s.Username
	airtime.Key = key
	airtime.Operator = airtimeReq.Operator
	airtime.MobileNo = airtimeReq.MobileNo
	airtime.Amount = airtimeReq.Amount

	body, err := json.Marshal(airtime)
	if err != nil {
		return "", nil
	}

	headers := make(map[string]string)
	headers["content-type"] = "application/json"
	headers["cache-control"] = "no-cache"

	url := s.baseURL() + "/airtimebuypinless"
	return s.newReq(url, body, headers)
}

// BuyAirtime buys Pinless Airtime
func (s Service) BuyAirtimeVoucher(airtimeVoucherRequest AirtimeVoucherReq) (string, error) {

	key, err := s.auth()
	if err != nil {
		return "", nil
	}

	airtime := AirtimeVoucher{}
	airtime.Username = s.Username
	airtime.Key = key
	airtime.Operator = airtimeVoucherRequest.Operator
	airtime.Ref = airtimeVoucherRequest.Ref
	airtime.Amount = airtimeVoucherRequest.Amount

	body, err := json.Marshal(airtime)
	if err != nil {
		return "", nil
	}

	headers := make(map[string]string)
	headers["content-type"] = "application/json"
	headers["cache-control"] = "no-cache"

	url := s.baseURL() + "/airtimebuy"
	return s.newReq(url, body, headers)
}

// newReq handles all POST requests
func (s Service) newReq(url string, body []byte, headers map[string]string) (string, error) {
	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	if err != nil {
		return "", nil
	}

	for key, value := range headers {
		request.Header.Set(key, value)
	}

	client := &http.Client{Timeout: 60 * time.Second}
	res, err := client.Do(request)
	if res != nil {
		defer res.Body.Close()
	}
	if err != nil {
		return "", err
	}

	stringBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	return string(stringBody), nil
}

// baseURL determines URL depending with Env¬
func (s Service) baseURL() string {
	if s.Env == PRODUCTION {
		return "https://vendingpointea.com:2053/v3/"
	}
	return "http://128.199.192.158:8787/v3/"
}
