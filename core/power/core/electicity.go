package core

import (
	"encoding/json"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"github.com/julienschmidt/httprouter"
	"github.com/negah/percent"
	"github.com/satori/go.uuid"
	"log"
	"math"
	kplc "mwangaza/core/power/api"
	"mwangaza/core/trans"
	"mwangaza/model"
	"mwangaza/settings"
	"net/http"
	"strconv"
	"strings"
)

// Handler is a route routes for requests
type Handler struct {
	database *gorm.DB
	power    kplc.Service
}

// New creates a new Handler
func New(database *gorm.DB, power kplc.Service) Handler {
	return Handler{database: database, power: power}
}

// VerifyPrepaid This verifies the KPLC Prepaid Meter Details
func (h Handler) VerifyPrepaid(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				var prepaid model.PrePaidRequest
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&prepaid)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				verifyRes, _ := h.power.VerifyPrePaidMeter(kplc.VerifyPrePaid{
					Meter: prepaid.MeterNumber,
				})

				if err != nil {
					log.Println(err)
				}

				var verify kplc.PrePaidRes
				err = json.Unmarshal([]byte(verifyRes), &verify)
				if err != nil {
					log.Println(err)
				}

				status := verify.Status

				if status == 0 {

					id := uuid.NewV4()

					res := h.database.Create(&model.PrePaid{PrePaidId: id, UserId: prepaid.UserId, MeterNumber: prepaid.MeterNumber, CustomerName: verify.CustomerName}).Scan(&model.PrePaid{})
					resp := res.Value
					actual := resp.(*model.PrePaid)

					var prePaidRes = model.PrePaidResponse{}
					prePaidRes.PrePaidId = actual.PrePaidId
					prePaidRes.UserId = actual.UserId
					prePaidRes.MeterNumber = actual.MeterNumber
					prePaidRes.CustomerName = actual.CustomerName

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusOK)
					json.NewEncoder(w).Encode(prePaidRes)
				} else if status == 1 {

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "Failed Request, Pending"})
				} else if status == 102 {

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "Insufficient Balance"})
				} else if status == 103 {

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "Bad Reference Number"})
				} else if status == 104 {

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "Transaction Not Found"})
				} else if status == 105 {

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "Meter reached maximum vends allowed per day"})
				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}
}

// PurchasePrepaid This purchases KPLC Prepaid Meter Token
func (h Handler) PurchasePrepaid(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)
				wallet := model.Wallet{}

				var prepaid model.PrePaidVend
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&prepaid)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				_ = h.database.Raw("SELECT * FROM wallets WHERE user_id = ?", claims["userId"]).Scan(&wallet)

				if !wallet.Mwangaza.Active {
					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "User not found"})
					return
				}

				if int(prepaid.Amount) <= wallet.Balance {

					vendRes, _ := h.power.VendPrePaid(kplc.PrePaidVend{
						Meter:        prepaid.MeterNumber,
						CustomerName: prepaid.CustomerName,
						Amount:       strconv.Itoa(int(prepaid.Amount)),
					})

					if err != nil {
						log.Println(err)
					}

					var vend kplc.PrePaidVendRes
					err = json.Unmarshal([]byte(vendRes), &vend)
					if err != nil {
						log.Println(err)
					}

					str := spew.Sdump(vend)
					fmt.Println(str)

					status := vend.Status

					if status == 0 {

						trans.New(h.database).CreateTransactionRecord(wallet.UserId, "KPLC PrePaid", int(prepaid.Amount))

						_ = h.database.Model(&model.Wallet{}).Where("user_id =?", claims["userId"]).UpdateColumn("balance", gorm.Expr("balance - ?", prepaid.Amount))

						var vendResponse model.PrePaid
						_ = h.database.Model(&model.PrePaid{}).Where("pre_paid_id = ?", prepaid.PrePaidId).
							Updates(model.PrePaid{TransactionStatus: true, Units: vend.Transaction.Units, ResCode: vend.Transaction.Rescode,
								StdTokenTax: vend.Transaction.StdTokenTax, StdTokenAmt: vend.Transaction.StdTokenAmt, Token: vend.Transaction.Token,
								StdTokenRctNum: vend.Transaction.StdTokenRctNum, Date: vend.Transaction.Date, TotalAmount: vend.Transaction.TotalAmount,
								Ref: vend.Transaction.Ref, StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						var prePaidVendRes = model.PrePaidVendResponse{}
						prePaidVendRes.PrePaidId = vendResponse.PrePaidId
						prePaidVendRes.UserId = vendResponse.UserId
						prePaidVendRes.MeterNumber = vendResponse.MeterNumber
						prePaidVendRes.CustomerName = vendResponse.CustomerName
						prePaidVendRes.Units = vendResponse.Units
						prePaidVendRes.Token = vendResponse.Token
						prePaidVendRes.TotalAmount = vendResponse.TotalAmount

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(prePaidVendRes)

					} else if status == 1 {
						if vend.Transaction.Status == 2 {
							var vendResponse model.PrePaid

							_ = h.database.Model(&model.PrePaid{}).Where("pre_paid_id = ?", prepaid.PrePaidId).
								Updates(model.PrePaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

							if err != nil {
								log.Println(err)
							}

							w.Header().Set("Content-Type", "application/json")
							w.WriteHeader(http.StatusUnauthorized)
							json.NewEncoder(w).Encode(model.Response{Message: vend.Transaction.Msg})
						} else {
							var vendResponse model.PrePaid

							_ = h.database.Model(&model.PrePaid{}).Where("pre_paid_id = ?", prepaid.PrePaidId).
								Updates(model.PrePaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

							if err != nil {
								log.Println(err)
							}

							w.Header().Set("Content-Type", "application/json")
							w.WriteHeader(http.StatusUnauthorized)
							json.NewEncoder(w).Encode(model.Response{Message: "Failed Request, Pending"})
						}

					} else if status == 102 {

						var vendResponse model.PrePaid

						_ = h.database.Model(&model.PrePaid{}).Where("pre_paid_id = ?", prepaid.PrePaidId).
							Updates(model.PrePaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Insufficient Balance"})
					} else if status == 103 {

						var vendResponse model.PrePaid

						_ = h.database.Model(&model.PrePaid{}).Where("pre_paid_id = ?", prepaid.PrePaidId).
							Updates(model.PrePaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Bad Reference Number"})
					} else if status == 104 {

						var vendResponse model.PrePaid

						_ = h.database.Model(&model.PrePaid{}).Where("pre_paid_id = ?", prepaid.PrePaidId).
							Updates(model.PrePaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Transaction Not Found"})
					} else if status == 105 {

						var vendResponse model.PrePaid

						_ = h.database.Model(&model.PrePaid{}).Where("pre_paid_id = ?", prepaid.PrePaidId).
							Updates(model.PrePaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Meter reached maximum vends allowed per day"})
					}

				} else {
					resp := new(model.Response)
					resp.Message = "Your Wallet Balance is Insufficient"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(resp)
				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func (h Handler) PurchasePrepaidShop(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)
				wallet := model.ShopWallet{}
				var bonus int

				var prepaid model.PrePaidVend
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&prepaid)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				bonus = getPercentage(int(prepaid.Amount))

				_ = h.database.Raw("SELECT * FROM shop_wallets WHERE shop_id = ?", claims["userId"]).Scan(&wallet)

				if !wallet.Mwangaza.Active {
					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "User not found"})
					return
				}

				if int(prepaid.Amount) <= wallet.Balance {

					vendRes, _ := h.power.VendPrePaid(kplc.PrePaidVend{
						Meter:        prepaid.MeterNumber,
						CustomerName: prepaid.CustomerName,
						Amount:       strconv.Itoa(int(prepaid.Amount)),
					})

					if err != nil {
						log.Println(err)
					}

					var vend kplc.PrePaidVendRes
					err = json.Unmarshal([]byte(vendRes), &vend)
					if err != nil {
						log.Println(err)
					}

					status := vend.Status

					if status == 0 {

						trans.New(h.database).AddTransactionBonus(wallet.ShopId, bonus)
						trans.New(h.database).CreateShopTransactionRecord(wallet.ShopId, "KPLC PrePaid", int(prepaid.Amount), bonus)

						_ = h.database.Model(&model.ShopWallet{}).Where("shop_id =?", claims["userId"]).UpdateColumn("balance", gorm.Expr("balance - ?", prepaid.Amount))

						var vendResponse model.PrePaid
						_ = h.database.Model(&model.PrePaid{}).Where("pre_paid_id = ?", prepaid.PrePaidId).
							Updates(model.PrePaid{TransactionStatus: true, Units: vend.Transaction.Units, ResCode: vend.Transaction.Rescode,
								StdTokenTax: vend.Transaction.StdTokenTax, StdTokenAmt: vend.Transaction.StdTokenAmt, Token: vend.Transaction.Token,
								StdTokenRctNum: vend.Transaction.StdTokenRctNum, Date: vend.Transaction.Date, TotalAmount: vend.Transaction.TotalAmount,
								Ref: vend.Transaction.Ref, StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						str := spew.Sdump(vend)
						fmt.Println(str)

						if err != nil {
							log.Println(err)
						}

						var prePaidVendRes = model.PrePaidVendResponse{}
						prePaidVendRes.PrePaidId = vendResponse.PrePaidId
						prePaidVendRes.UserId = vendResponse.UserId
						prePaidVendRes.MeterNumber = vendResponse.MeterNumber
						prePaidVendRes.CustomerName = vendResponse.CustomerName
						prePaidVendRes.Units = vendResponse.Units
						prePaidVendRes.Token = vendResponse.Token
						prePaidVendRes.TotalAmount = vendResponse.TotalAmount

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(prePaidVendRes)

					} else if status == 1 {
						if vend.Transaction.Status == 2 {
							var vendResponse model.PrePaid

							_ = h.database.Model(&model.PrePaid{}).Where("pre_paid_id = ?", prepaid.PrePaidId).
								Updates(model.PrePaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

							if err != nil {
								log.Println(err)
							}

							w.Header().Set("Content-Type", "application/json")
							w.WriteHeader(http.StatusUnauthorized)
							json.NewEncoder(w).Encode(model.Response{Message: vend.Transaction.Msg})
						} else {
							var vendResponse model.PrePaid

							_ = h.database.Model(&model.PrePaid{}).Where("pre_paid_id = ?", prepaid.PrePaidId).
								Updates(model.PrePaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

							if err != nil {
								log.Println(err)
							}

							w.Header().Set("Content-Type", "application/json")
							w.WriteHeader(http.StatusUnauthorized)
							json.NewEncoder(w).Encode(model.Response{Message: "Failed Request, Pending"})
						}

					} else if status == 102 {

						var vendResponse model.PrePaid

						_ = h.database.Model(&model.PrePaid{}).Where("pre_paid_id = ?", prepaid.PrePaidId).
							Updates(model.PrePaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Insufficient Balance"})
					} else if status == 103 {

						var vendResponse model.PrePaid

						_ = h.database.Model(&model.PrePaid{}).Where("pre_paid_id = ?", prepaid.PrePaidId).
							Updates(model.PrePaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Bad Reference Number"})
					} else if status == 104 {

						var vendResponse model.PrePaid

						_ = h.database.Model(&model.PrePaid{}).Where("pre_paid_id = ?", prepaid.PrePaidId).
							Updates(model.PrePaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Transaction Not Found"})
					} else if status == 105 {

						var vendResponse model.PrePaid

						_ = h.database.Model(&model.PrePaid{}).Where("pre_paid_id = ?", prepaid.PrePaidId).
							Updates(model.PrePaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Meter reached maximum vends allowed per day"})
					}

				} else {
					resp := new(model.Response)
					resp.Message = "Your Wallet Balance is Insufficient"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(resp)
				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func (h Handler) FetchRecentTokens(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)

				rows, _ := h.database.Limit(10).Raw("SELECT * FROM pre_paids WHERE user_id = ? AND transaction_status = ? ORDER BY created_at DESC", claims["userId"], true).Rows()
				var transRes []model.PrePaid
				for rows.Next() {
					var tran model.PrePaid
					h.database.ScanRows(rows, &tran)
					transRes = append(transRes, tran)
				}

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				json.NewEncoder(w).Encode(transRes)

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

// VerifyPostpaid This verifies the KPLC Postpaid Meter Details
func (h Handler) VerifyPostpaid(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				var postpaid model.PostPaidRequest
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&postpaid)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				verifyRes, _ := h.power.VerifyPostPaidMeter(kplc.VerifyPostPaid{
					AccountNo: postpaid.AccountNumber,
				})

				if err != nil {
					log.Println(err)
				}

				var verify kplc.PostPaidRes
				err = json.Unmarshal([]byte(verifyRes), &verify)
				if err != nil {
					log.Println(err)
				}

				status := verify.Status

				if status == 0 {

					id := uuid.NewV4()

					res := h.database.Create(&model.PostPaid{PostPaidId: id, UserId: postpaid.UserId, AccountNumber: postpaid.AccountNumber, CustomerName: verify.CustomerName, CustomerBalance: verify.CustomerBalance}).Scan(&model.PostPaid{})
					resp := res.Value
					actual := resp.(*model.PostPaid)

					var postPaidRes = model.PostPaidResponse{}
					postPaidRes.PostPaidId = actual.PostPaidId
					postPaidRes.UserId = actual.UserId
					postPaidRes.AccountNumber = actual.AccountNumber
					postPaidRes.CustomerName = actual.CustomerName
					postPaidRes.CustomerBalance = actual.CustomerBalance

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusOK)
					json.NewEncoder(w).Encode(postPaidRes)
				} else if status == 1 {

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "Failed Request, Pending"})
				} else if status == 102 {

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "Insufficient Balance"})
				} else if status == 103 {

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "Bad Reference Number"})
				} else if status == 104 {

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "Transaction Not Found"})
				} else if status == 105 {

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "Meter reached maximum vends allowed per day"})
				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}
}

// PurchasePostpaid This pays the KPLC Postpaid Meter Bill
func (h Handler) PurchasePostpaid(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)
				wallet := model.Wallet{}

				var postpaid model.PostPaidVend
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&postpaid)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				_ = h.database.Raw("SELECT * FROM wallets WHERE user_id = ?", claims["userId"]).Scan(&wallet)

				if !wallet.Mwangaza.Active {
					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "User not found"})
					return
				}

				if int(postpaid.Amount) <= wallet.Balance {

					vendRes, _ := h.power.VendPostPaid(kplc.PostPaidVend{
						AccountNo:    postpaid.AccountNumber,
						CustomerName: postpaid.CustomerName,
						Amount:       strconv.Itoa(int(postpaid.Amount)),
					})

					if err != nil {
						log.Println(err)
					}

					var vend kplc.PostPaidVendRes
					err = json.Unmarshal([]byte(vendRes), &vend)
					if err != nil {
						log.Println(err)
					}

					status := vend.Status

					if status == 0 {

						trans.New(h.database).CreateTransactionRecord(wallet.UserId, "KPLC PostPaid", int(postpaid.Amount))

						_ = h.database.Model(&model.Wallet{}).Where("user_id =?", claims["userId"]).UpdateColumn("balance", gorm.Expr("balance - ?", postpaid.Amount))

						var vendResponse model.PostPaid

						_ = h.database.Model(&model.PostPaid{}).Where("post_paid_id = ?", postpaid.PostPaidId).
							Updates(model.PostPaid{Amount: vend.Transaction.Amount, TransactionStatus: true, RctNum: vend.Transaction.RctNum, Date: vend.Transaction.Date,
								TransId: vend.Transaction.TransactionID, Ref: vend.Transaction.Ref, StatusCode: vend.Status,
								StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						var postPaidVendRes = model.PostPaidVendResponse{}
						postPaidVendRes.PostPaidId = vendResponse.PostPaidId
						postPaidVendRes.UserId = vendResponse.UserId
						postPaidVendRes.AccountNumber = vendResponse.AccountNumber
						postPaidVendRes.CustomerName = vendResponse.CustomerName
						postPaidVendRes.RctNum = vendResponse.RctNum
						postPaidVendRes.Amount = vendResponse.Amount
						postPaidVendRes.TransId = vendResponse.TransId
						postPaidVendRes.Ref = vendResponse.Ref

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(postPaidVendRes)
					} else if status == 1 {

						var vendResponse model.PostPaid

						_ = h.database.Model(&model.PostPaid{}).Where("post_paid_id = ?", postpaid.PostPaidId).
							Updates(model.PostPaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Failed Request, Pending"})
					} else if status == 102 {

						var vendResponse model.PostPaid

						_ = h.database.Model(&model.PostPaid{}).Where("post_paid_id = ?", postpaid.PostPaidId).
							Updates(model.PostPaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Insufficient Balance"})
					} else if status == 103 {

						var vendResponse model.PostPaid

						_ = h.database.Model(&model.PostPaid{}).Where("post_paid_id = ?", postpaid.PostPaidId).
							Updates(model.PostPaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Bad Reference Number"})
					} else if status == 104 {

						var vendResponse model.PostPaid

						_ = h.database.Model(&model.PostPaid{}).Where("post_paid_id = ?", postpaid.PostPaidId).
							Updates(model.PostPaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Transaction Not Found"})
					} else if status == 105 {

						var vendResponse model.PostPaid

						_ = h.database.Model(&model.PostPaid{}).Where("post_paid_id = ?", postpaid.PostPaidId).
							Updates(model.PostPaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Meter reached maximum vends allowed per day"})
					}

				} else {
					resp := new(model.Response)
					resp.Message = "Your Wallet Balance is Insufficient"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(resp)
				}
			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func (h Handler) PurchasePostpaidShop(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)
				wallet := model.ShopWallet{}

				var postpaid model.PostPaidVend
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&postpaid)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				_ = h.database.Raw("SELECT * FROM shop_wallets WHERE shop_id = ?", claims["userId"]).Scan(&wallet)

				if !wallet.Mwangaza.Active {
					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "User not found"})
					return
				}

				if int(postpaid.Amount) <= wallet.Balance {

					vendRes, _ := h.power.VendPostPaid(kplc.PostPaidVend{
						AccountNo:    postpaid.AccountNumber,
						CustomerName: postpaid.CustomerName,
						Amount:       strconv.Itoa(int(postpaid.Amount)),
					})

					if err != nil {
						log.Println(err)
					}

					var vend kplc.PostPaidVendRes
					err = json.Unmarshal([]byte(vendRes), &vend)
					if err != nil {
						log.Println(err)
					}

					status := vend.Status

					if status == 0 {

						trans.New(h.database).CreateTransactionRecord(wallet.ShopId, "KPLC PostPaid", int(postpaid.Amount))

						_ = h.database.Model(&model.ShopWallet{}).Where("shop_id =?", claims["userId"]).UpdateColumn("balance", gorm.Expr("balance - ?", postpaid.Amount))

						var vendResponse model.PostPaid

						_ = h.database.Model(&model.PostPaid{}).Where("post_paid_id = ?", postpaid.PostPaidId).
							Updates(model.PostPaid{Amount: vend.Transaction.Amount, TransactionStatus: true, RctNum: vend.Transaction.RctNum, Date: vend.Transaction.Date,
								TransId: vend.Transaction.TransactionID, Ref: vend.Transaction.Ref, StatusCode: vend.Status,
								StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						var postPaidVendRes = model.PostPaidVendResponse{}
						postPaidVendRes.PostPaidId = vendResponse.PostPaidId
						postPaidVendRes.UserId = vendResponse.UserId
						postPaidVendRes.AccountNumber = vendResponse.AccountNumber
						postPaidVendRes.CustomerName = vendResponse.CustomerName
						postPaidVendRes.RctNum = vendResponse.RctNum
						postPaidVendRes.Amount = vendResponse.Amount
						postPaidVendRes.TransId = vendResponse.TransId
						postPaidVendRes.Ref = vendResponse.Ref

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(postPaidVendRes)
					} else if status == 1 {

						var vendResponse model.PostPaid

						_ = h.database.Model(&model.PostPaid{}).Where("post_paid_id = ?", postpaid.PostPaidId).
							Updates(model.PostPaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Failed Request, Pending"})
					} else if status == 102 {

						var vendResponse model.PostPaid

						_ = h.database.Model(&model.PostPaid{}).Where("post_paid_id = ?", postpaid.PostPaidId).
							Updates(model.PostPaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Insufficient Balance"})
					} else if status == 103 {

						var vendResponse model.PostPaid

						_ = h.database.Model(&model.PostPaid{}).Where("post_paid_id = ?", postpaid.PostPaidId).
							Updates(model.PostPaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Bad Reference Number"})
					} else if status == 104 {

						var vendResponse model.PostPaid

						_ = h.database.Model(&model.PostPaid{}).Where("post_paid_id = ?", postpaid.PostPaidId).
							Updates(model.PostPaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Transaction Not Found"})
					} else if status == 105 {

						var vendResponse model.PostPaid

						_ = h.database.Model(&model.PostPaid{}).Where("post_paid_id = ?", postpaid.PostPaidId).
							Updates(model.PostPaid{StatusCode: vend.Status, StatusMessage: vend.Msg}).Scan(&vendResponse)

						if err != nil {
							log.Println(err)
						}

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "Meter reached maximum vends allowed per day"})
					}

				} else {
					resp := new(model.Response)
					resp.Message = "Your Wallet Balance is Insufficient"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(resp)
				}
			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func getPercentage(amount int) int {
	res := percent.Percent(2, amount)
	return int(math.Round(res))
}
