package whatsapp

import (
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/jinzhu/gorm"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"net/http/httputil"
)

// Handler is a route routes for requests
type Handler struct {
	database *gorm.DB
}

// New creates a new Handler
func New(database *gorm.DB) Handler {
	return Handler{database: database}
}

func (h Handler) TwillioMessage(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	requestDump, err := httputil.DumpRequest(r, true)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(requestDump))
}

func (h Handler) TwillioCallback(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	str := spew.Sdump(r.Body)
	fmt.Println(str)
}
