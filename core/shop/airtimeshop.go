package shop

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"github.com/julienschmidt/httprouter"
	"github.com/negah/percent"
	"github.com/satori/go.uuid"
	"log"
	"math"
	"math/rand"
	kplc "mwangaza/core/power/api"
	"mwangaza/core/trans"
	"mwangaza/model"
	"mwangaza/settings"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// Handler is a route routes for requests
type Handler struct {
	database *gorm.DB
	power    kplc.Service
}

// New creates a new Handler
func New(database *gorm.DB, power kplc.Service) Handler {
	return Handler{database: database, power: power}
}

// ---------------------------------------------------------------------------------------------------------------------
// BUY AIRTIME
// ---------------------------------------------------------------------------------------------------------------------

func (h Handler) BuyShopAirtimeVoucher(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)
				wallet := model.ShopWallet{}

				var airtime model.AirtimeVoucherRequest
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&airtime)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				_ = h.database.Raw("SELECT * FROM shop_wallets WHERE shop_id = ?", claims["userId"]).Scan(&wallet)

				if !wallet.Mwangaza.Active {
					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "User not found"})
					return
				}

				if int(airtime.Amount) <= wallet.Balance {

					var totalAmount int
					var bonus int
					ref := "MGZ-" + refRandSeq(8)

					bonus = getPercentage(int(airtime.Amount))
					totalAmount = int(airtime.Amount)

					airtimeRes, _ := h.power.BuyAirtimeVoucher(kplc.AirtimeVoucherReq{
						Operator: airtime.Operator,
						Amount:   strconv.Itoa(totalAmount),
						Ref:      ref,
					})

					if err != nil {
						log.Println(err)
					}

					var verify kplc.AirtimeVoucherRes
					err = json.Unmarshal([]byte(airtimeRes), &verify)
					if err != nil {
						log.Println(err)
					}

					fmt.Println(airtimeRes)

					status := verify.Transaction.Status
					if status == 1 {

						trans.New(h.database).AddTransactionBonus(wallet.ShopId, bonus)
						trans.New(h.database).CreateShopTransactionRecord(wallet.ShopId, "Airtime Voucher", int(airtime.Amount), bonus)

						_ = h.database.Model(&model.ShopWallet{}).Where("shop_id =?", claims["userId"]).UpdateColumn("balance", gorm.Expr("balance - ?", airtime.Amount))

						id := uuid.NewV4()
						res := h.database.Create(&model.ShopAirtimeVoucher{AirtimeId: id, ShopId: wallet.ShopId, Ref: ref,
							Amount: totalAmount, Operator: airtime.Operator, Pincode: verify.Transaction.Pincode,
							Date: verify.Transaction.Date, TransactionId: verify.Transaction.TransactionID, TransactionStatus: true,
							StatusCode: verify.Status, StatusMessage: verify.Msg}).Scan(&model.Airtime{})
						resp := res.Value
						actual := resp.(*model.ShopAirtimeVoucher)

						var airtimeResponse = model.ShopAirtimeVoucherResponse{}
						airtimeResponse.AirtimeId = actual.AirtimeId
						airtimeResponse.ShopId = actual.ShopId
						airtimeResponse.Ref = actual.Ref
						airtimeResponse.Amount = actual.Amount
						airtimeResponse.Pincode = actual.Pincode
						airtimeResponse.TransactionStatus = actual.TransactionStatus

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(airtimeResponse)

					} else if status == 2 {

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(model.Response{Message: "Voucher is out of stock"})
					} else if status == 102 {

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(model.Response{Message: "Insufficient Balance"})
					} else if status == 103 {

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(model.Response{Message: "Bad Reference Number"})
					} else if status == 104 {

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(model.Response{Message: "Transaction Not Found"})
					} else {

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(model.Response{Message: "An error occured"})
					}

				} else {
					resp := new(model.Response)
					resp.Message = "Your Wallet Balance is Insufficient"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(resp)
				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func (h Handler) BuyShopAirtime(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)
				wallet := model.ShopWallet{}

				var airtime model.AirtimeRequest
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&airtime)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				_ = h.database.Raw("SELECT * FROM shop_wallets WHERE shop_id = ?", claims["userId"]).Scan(&wallet)

				if !wallet.Mwangaza.Active {
					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "User not found"})
					return
				}

				if int(airtime.Amount) <= wallet.Balance {

					var totalAmount int
					var bonus int

					bonus = getPercentage(int(airtime.Amount))
					totalAmount = int(airtime.Amount)

					phones := strings.Replace(airtime.PhoneNumber, " ", "", -1)
					fmt.Println("The Phones Number is ", phones)

					var phoneNumber string
					if strings.HasPrefix(phones, "07") {
						phoneNumber = "254" + phones[1:]
					} else {
						phoneNumber = phones
					}

					var phone string
					if strings.HasPrefix(phoneNumber, "254") {
						phone = phoneNumber[0:6]
					}

					operator := getOperatorFromPrefix(phone)

					fmt.Println("The Phones Number is ", phoneNumber)
					fmt.Println("The Operator is ", operator)

					airtimeRes, _ := h.power.BuyAirtime(kplc.AirtimeReq{
						Operator: operator,
						MobileNo: phoneNumber,
						Amount:   strconv.Itoa(totalAmount),
					})

					if err != nil {
						log.Println(err)
					}

					var verify kplc.AirtimeRes
					err = json.Unmarshal([]byte(airtimeRes), &verify)
					if err != nil {
						log.Println(err)
					}

					status := verify.Status
					if status == 0 {

						status := verify.Transaction.Status
						if status == 1 {

							trans.New(h.database).AddTransactionBonus(wallet.ShopId, bonus)
							trans.New(h.database).CreateShopTransactionRecord(wallet.ShopId, "Airtime", int(airtime.Amount), bonus)

							_ = h.database.Model(&model.ShopWallet{}).Where("shop_id =?", claims["userId"]).UpdateColumn("balance", gorm.Expr("balance - ?", airtime.Amount))

							id := uuid.NewV4()
							res := h.database.Create(&model.ShopAirtime{AirtimeId: id, ShopId: wallet.ShopId, PhoneNumber: phones,
								Amount: totalAmount, Operator: airtime.Operator, ReceiptNumber: verify.Transaction.Receiptno,
								Date: verify.Transaction.Date, TransactionId: verify.Transaction.TransactionID, TransactionStatus: true,
								StatusCode: verify.Status, StatusMessage: verify.Msg}).Scan(&model.Airtime{})
							resp := res.Value
							actual := resp.(*model.ShopAirtime)

							var airtimeResponse = model.ShopAirtimeResponse{}
							airtimeResponse.AirtimeId = actual.AirtimeId
							airtimeResponse.ShopId = actual.ShopId
							airtimeResponse.PhoneNumber = actual.PhoneNumber
							airtimeResponse.Amount = actual.Amount
							airtimeResponse.ReceiptNumber = actual.ReceiptNumber
							airtimeResponse.TransactionStatus = actual.TransactionStatus

							w.Header().Set("Content-Type", "application/json")
							w.WriteHeader(http.StatusOK)
							json.NewEncoder(w).Encode(airtimeResponse)
						} else {

							w.Header().Set("Content-Type", "application/json")
							w.WriteHeader(http.StatusOK)
							json.NewEncoder(w).Encode(model.Response{Message: "Failed Request"})
						}

					} else if status == 1 {

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(model.Response{Message: "Failed Request, Pending"})
					} else if status == 102 {

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(model.Response{Message: "Insufficient Balance"})
					} else if status == 103 {

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(model.Response{Message: "Bad Reference Number"})
					} else if status == 104 {

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(model.Response{Message: "Transaction Not Found"})
					} else if status == 105 {

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(model.Response{Message: "Meter reached maximum vends allowed per day"})
					}

				} else {
					resp := new(model.Response)
					resp.Message = "Your Wallet Balance is Insufficient"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(resp)
				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func getPercentage(amount int) int {
	res := percent.Percent(6, amount)
	return int(math.Round(res))
}

func getOperatorFromPrefix(pn string) string {
	//if pn == "254700" || pn == "254701" || pn == "254702" || pn == "254703" || pn == "254704" || pn == "254705" || pn == "254706" || pn == "254707" || pn == "254708" || pn == "254709" || pn == "254710" || pn == "254711" || pn == "254712" || pn == "254713" || pn == "254714" || pn == "254715" || pn == "254716" || pn == "254717" || pn == "254718" || pn == "254719" || pn == "254720" || pn == "254721" || pn == "254722" || pn == "254723" || pn == "254724" || pn == "254725" || pn == "254726" || pn == "254727" || pn == "254728" || pn == "254729" || pn == "254790" || pn == "254791" || pn == "254792" || pn == "254793" || pn == "254794" || pn == "254795" || pn == "254796" || pn == "254797" || pn == "254798" || pn == "254799" || pn == "254740" || pn == "254741" || pn == "254742" || pn == "254743" || pn == "254745" || pn == "254746" || pn == "254748" || pn == "254746" || pn == "254757" || pn == "254758" || pn == "254759" {
	if pn == "254700" || pn == "254701" || pn == "254702" || pn == "254703" || pn == "254704" || pn == "254705" || pn == "254706" || pn == "254707" || pn == "254708" || pn == "254709" || pn == "254710" || pn == "254711" || pn == "254712" || pn == "254713" || pn == "254714" || pn == "254715" || pn == "254716" || pn == "254717" || pn == "254718" || pn == "254719" || pn == "254720" || pn == "254721" || pn == "254722" || pn == "254723" || pn == "254724" || pn == "254725" || pn == "254726" || pn == "254727" || pn == "254728" || pn == "254729" || pn == "254790" || pn == "254791" || pn == "254792" || pn == "254793" || pn == "254794" || pn == "254795" || pn == "254796" || pn == "254797" || pn == "254798" || pn == "254799" || pn == "254740" || pn == "254741" || pn == "254742" || pn == "254743" || pn == "254745" || pn == "254748" || pn == "254746" || pn == "254757" || pn == "254758" || pn == "254759" {
		return "safaricom"
	} else if pn == "254731" || pn == "254732" || pn == "254733" || pn == "254734" || pn == "254735" || pn == "254736" || pn == "254737" || pn == "254738" || pn == "254739" || pn == "254750" || pn == "254751" || pn == "254752" || pn == "254753" || pn == "254754" || pn == "254755" || pn == "254756" || pn == "254780" || pn == "254781" || pn == "254782" || pn == "254783" || pn == "254784" || pn == "254785" || pn == "254786" || pn == "254787" || pn == "254788" || pn == "254789" {
		return "airtel"
	} else if pn == "254770" || pn == "254771" || pn == "254772" || pn == "254773" || pn == "254774" || pn == "254775" || pn == "254776" || pn == "254777" || pn == "254778" || pn == "254779" {
		return "telkom"
	} else if pn == "254763" || pn == "254764" || pn == "254765" {
		return "equitel"
	} else if pn == "254747" {
		return "faiba4g"
	}
	return ""
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

var refLetters = []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789")

func refRandSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = refLetters[rand.Intn(len(refLetters))]
	}
	return string(b)
}
