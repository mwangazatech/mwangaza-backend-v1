package tv

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"github.com/julienschmidt/httprouter"
	"github.com/satori/go.uuid"
	"mwangaza/core/trans"
	"mwangaza/model"
	"mwangaza/settings"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

// Handler is a route routes for requests
type Handler struct {
	database *gorm.DB
}

// New creates a new Handler
func New(database *gorm.DB) Handler {
	return Handler{database: database}
}

const (
	apiUrl = "https://apis.ipayafrica.com"
	secret = "mWh874SAn458CR"
)

// ---------------------------------------------------------------------------------------------------------------------
// T.V AND INTERNET
// ---------------------------------------------------------------------------------------------------------------------

func (handler Handler) PayUtilities(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)
				wallet := model.Wallet{}

				var utility model.UtilitiesRequest
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&utility)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				_ = handler.database.Raw("SELECT * FROM wallets WHERE user_id = ?", claims["userId"]).Scan(&wallet)

				if !wallet.Mwangaza.Active {
					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "User not found"})
					return
				}

				if int(int(utility.Amount)) <= wallet.Balance {

					hashData := "account=" + utility.Account + "&amount=" + strconv.Itoa(int(int(utility.Amount))) + "&biller_name=" + utility.BillerName + "&merchant_reference=" + utility.MerchantReference + "&phone=" + utility.Phone + "&vid=mwangaza"
					// Create a new HMAC by defining the hash type and the key (as byte array)
					h := hmac.New(sha256.New, []byte(secret))
					// Write Data to it
					h.Write([]byte(hashData))
					// Get result and encode as hexadecimal string
					sha := hex.EncodeToString(h.Sum(nil))

					resource := "/ipay-billing/transaction/create"
					data := url.Values{}
					data.Set("account", utility.Account)
					data.Set("amount", strconv.Itoa(int(int(utility.Amount))))
					data.Set("biller_name", utility.BillerName)
					data.Set("merchant_reference", utility.MerchantReference)
					data.Set("phone", utility.Phone)
					data.Set("vid", "mwangaza")
					data.Set("hash", sha)

					u, _ := url.ParseRequestURI(apiUrl)
					u.Path = resource
					urlStr := u.String()

					client := &http.Client{}
					r, _ := http.NewRequest("POST", urlStr, strings.NewReader(data.Encode())) // URL-encoded payload
					r.Header.Add("Content-Type", "application/x-www-form-urlencoded")

					resp, err := client.Do(r)
					if err != nil {
						fmt.Println(err)
					}

					if resp.StatusCode == 200 {

						trans.New(handler.database).CreateTransactionRecord(wallet.UserId, utility.BillerName, int(utility.Amount))
						_ = handler.database.Model(&model.Wallet{}).Where("user_id =?", claims["userId"]).UpdateColumn("balance", gorm.Expr("balance - ?", int(utility.Amount)))

						id := uuid.NewV4()
						res := handler.database.Create(&model.Utility{UtilityId: id, UserId: wallet.UserId, Account: utility.Account, Amount: int(utility.Amount),
							BillerName: utility.BillerName, MerchantReference: utility.MerchantReference, Phone: utility.Phone, Status: true}).Scan(&model.Utility{})

						resp := res.Value
						actual := resp.(*model.Utility)

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(actual)
					} else {
						id := uuid.NewV4()
						res := handler.database.Create(&model.Utility{UtilityId: id, UserId: wallet.UserId, Account: utility.Account, Amount: int(utility.Amount),
							BillerName: utility.BillerName, MerchantReference: utility.MerchantReference, Phone: utility.Phone, Status: false}).Scan(&model.Utility{})

						resp := res.Value
						actual := resp.(*model.Utility)

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(actual)
					}

				} else {
					resp := new(model.Response)
					resp.Message = "Your Wallet Balance is Insufficient"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(resp)
				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func (handler Handler) PayShopUtilities(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)
				wallet := model.ShopWallet{}

				var utility model.UtilitiesRequest
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&utility)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				_ = handler.database.Raw("SELECT * FROM shop_wallets WHERE shop_id = ?", claims["userId"]).Scan(&wallet)

				if !wallet.Mwangaza.Active {
					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "User not found"})
					return
				}

				if int(utility.Amount) <= wallet.Balance {

					hashData := "account=" + utility.Account + "&amount=" + strconv.Itoa(int(utility.Amount)) + "&biller_name=" + utility.BillerName + "&merchant_reference=" + utility.MerchantReference + "&phone=" + utility.Phone + "&vid=mwangaza"
					// Create a new HMAC by defining the hash type and the key (as byte array)
					h := hmac.New(sha256.New, []byte(secret))
					// Write Data to it
					h.Write([]byte(hashData))
					// Get result and encode as hexadecimal string
					sha := hex.EncodeToString(h.Sum(nil))

					resource := "/ipay-billing/transaction/create"
					data := url.Values{}
					data.Set("account", utility.Account)
					data.Set("amount", strconv.Itoa(int(utility.Amount)))
					data.Set("biller_name", utility.BillerName)
					data.Set("merchant_reference", utility.MerchantReference)
					data.Set("phone", utility.Phone)
					data.Set("vid", "mwangaza")
					data.Set("hash", sha)

					u, _ := url.ParseRequestURI(apiUrl)
					u.Path = resource
					urlStr := u.String()

					client := &http.Client{}
					r, _ := http.NewRequest("POST", urlStr, strings.NewReader(data.Encode())) // URL-encoded payload
					r.Header.Add("Content-Type", "application/x-www-form-urlencoded")

					resp, err := client.Do(r)
					if err != nil {
						fmt.Println(err)
					}

					if resp.StatusCode == 200 {

						trans.New(handler.database).CreateTransactionRecord(wallet.ShopId, utility.BillerName, int(utility.Amount))
						_ = handler.database.Model(&model.ShopWallet{}).Where("shop_id =?", claims["userId"]).UpdateColumn("balance", gorm.Expr("balance - ?", int(utility.Amount)))

						id := uuid.NewV4()
						res := handler.database.Create(&model.Utility{UtilityId: id, UserId: wallet.ShopId, Account: utility.Account, Amount: int(utility.Amount),
							BillerName: utility.BillerName, MerchantReference: utility.MerchantReference, Phone: utility.Phone, Status: true}).Scan(&model.Utility{})

						resp := res.Value
						actual := resp.(*model.Utility)

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(actual)
					} else {
						id := uuid.NewV4()
						res := handler.database.Create(&model.Utility{UtilityId: id, UserId: wallet.ShopId, Account: utility.Account, Amount: int(utility.Amount),
							BillerName: utility.BillerName, MerchantReference: utility.MerchantReference, Phone: utility.Phone, Status: false}).Scan(&model.Utility{})

						resp := res.Value
						actual := resp.(*model.Utility)

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(actual)
					}

				} else {
					resp := new(model.Response)
					resp.Message = "Your Wallet Balance is Insufficient"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(resp)
				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}
