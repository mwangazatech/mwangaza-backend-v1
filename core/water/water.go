package water

import (
	"encoding/json"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"github.com/julienschmidt/httprouter"
	"github.com/satori/go.uuid"
	"log"
	"mwangaza/core/water/api"
	"mwangaza/model"
	"mwangaza/settings"
	"net/http"
	"strings"
	"time"
)

// Handler is a route routes for requests
type Handler struct {
	database *gorm.DB
	water    api.Service
}

// New creates a new Handler
func New(database *gorm.DB, water api.Service) Handler {
	return Handler{database: database, water: water}
}

// ---------------------------------------------------------------------------------------------------------------------
// WATER BILL && PAYMENT
// ---------------------------------------------------------------------------------------------------------------------

func (h Handler) FetchNairobiWaterBill(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				_ = json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				var water model.WaterRequest
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&water)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				verifyRes, _ := h.water.NairobiBalance(api.VerifyWaterBill{
					AccountNumber: water.AccountNumber,
				})

				if err != nil {
					log.Println(err)
				}

				var verify api.NairobiWaterRes
				err = json.Unmarshal([]byte(verifyRes), &verify)
				if err != nil {
					log.Println(err)
				}

				status := verify.Status

				if status == 0 {

					id := uuid.NewV4()

					res := h.database.Create(&model.Water{WaterId: id, UserId: water.UserId, AccountNumber: water.AccountNumber, Amount: int(verify.Response.Amount), CustomerName: verify.Response.Names, DueDate: verify.Response.DueDate}).Scan(&model.Water{})
					resp := res.Value
					actual := resp.(*model.Water)

					var waterRes = model.WaterResponse{}
					waterRes.WaterId = actual.WaterId
					waterRes.UserId = actual.UserId
					waterRes.AccountNumber = actual.AccountNumber
					waterRes.CustomerName = actual.CustomerName
					waterRes.DueDate = actual.DueDate
					waterRes.Amount = actual.Amount

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusOK)
					_ = json.NewEncoder(w).Encode(waterRes)
				} else if status == 1 {

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					_ = json.NewEncoder(w).Encode(model.Response{Message: "Failed Request, Pending"})
				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				_ = json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		_ = json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func (h Handler) FetchKisumuWaterBill(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				_ = json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				var water model.WaterRequest
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&water)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				verifyRes, _ := h.water.KisumuBalance(api.VerifyWaterBill{
					AccountNumber: water.AccountNumber,
				})

				if err != nil {
					log.Println(err)
				}

				var verify api.KisumuWaterRes
				err = json.Unmarshal([]byte(verifyRes), &verify)
				if err != nil {
					log.Println(err)
				}

				status := verify.Status

				if status == 0 {

					id := uuid.NewV4()

					res := h.database.Create(&model.Water{WaterId: id, UserId: water.UserId, AccountNumber: water.AccountNumber, Amount: int(verify.Response.Amount), CustomerName: verify.Response.CustomerNames, DueDate: time.Now()}).Scan(&model.Water{})
					resp := res.Value
					actual := resp.(*model.Water)

					var waterRes = model.WaterResponse{}
					waterRes.WaterId = actual.WaterId
					waterRes.UserId = actual.UserId
					waterRes.AccountNumber = actual.AccountNumber
					waterRes.CustomerName = actual.CustomerName
					waterRes.DueDate = actual.DueDate
					waterRes.Amount = actual.Amount

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusOK)
					_ = json.NewEncoder(w).Encode(waterRes)
				} else if status == 1 {

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					_ = json.NewEncoder(w).Encode(model.Response{Message: "Failed Request, Pending"})
				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				_ = json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		_ = json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func (h Handler) PayWaterBill(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				_ = json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)
				wallet := model.Wallet{}

				var water model.WaterPresentation
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&water)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}
				_ = h.database.Raw("SELECT * FROM wallets WHERE user_id = ?", claims["userId"]).Scan(&wallet)

				if !wallet.Mwangaza.Active {
					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "User not found"})
					return
				}

				if int(water.Amount) <= wallet.Balance {

					var resp = &model.Water{}
					_ = h.database.Model(&model.Water{}).Where("water_id = ?", water.WaterId).UpdateColumn("paid", water.Amount).Scan(resp)

					str := spew.Sdump(resp)
					fmt.Println(str)

					var waterRes = model.WaterBill{}
					waterRes.WaterId = resp.WaterId
					waterRes.AccountNumber = resp.AccountNumber
					waterRes.Amount = string(resp.Amount)
					waterRes.Paid = resp.Paid

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusOK)
					_ = json.NewEncoder(w).Encode(waterRes)

				} else {
					resp := new(model.Response)
					resp.Message = "Your Wallet Balance is Insufficient"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					_ = json.NewEncoder(w).Encode(resp)
				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				_ = json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		_ = json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}
