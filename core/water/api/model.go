package api

import "time"

type Auth struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type AuthRes struct {
	Status  int    `json:"status"`
	Msg     string `json:"msg"`
	Balance int    `json:"balance"`
	Key     string `json:"key"`
}

type Balance struct {
	Username      string `json:"username"`
	Key           string `json:"key"`
	AccountNumber string `json:"account_number"`
}

type VerifyWaterBill struct {
	AccountNumber string `json:"account_number"`
}

type NairobiWaterRes struct {
	Status   int    `json:"status"`
	Msg      string `json:"msg"`
	Response struct {
		Amount        float64   `json:"Amount"`
		ServiceNumber int       `json:"ServiceNumber"`
		DueDate       time.Time `json:"DueDate"`
		Names         string    `json:"Names"`
	} `json:"response"`
}

type KisumuWaterRes struct {
	Status   int    `json:"status"`
	Msg      string `json:"msg"`
	Response struct {
		ChargeTypeID  int     `json:"ChargeTypeID"`
		AccountNumber string  `json:"AccountNumber"`
		CustomerNames string  `json:"CustomerNames"`
		Amount        float64 `json:"Amount"`
	} `json:"response"`
}
