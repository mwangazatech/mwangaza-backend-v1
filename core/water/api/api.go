package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// Env is the environment type
type Env string

const (
	// SANDBOX is the sandbox env tag
	SANDBOX = iota
	// PRODUCTION is the production env tag
	PRODUCTION
)

// Service is an TidalSpace Service
type Service struct {
	Username string
	Password string
	Env      int
}

// New return a new TidalSpace Service
func New(username, password string, env int) (Service, error) {
	return Service{username, password, env}, nil
}

//Generate TidalSpace Access Key
func (s Service) auth() (string, error) {

	auth := Auth{}
	auth.Username = s.Username
	auth.Password = s.Password

	body, err := json.Marshal(auth)
	reader := bytes.NewReader(body)
	if err != nil {
		return "", nil
	}

	url := s.baseURL() + "login"

	req, err := http.NewRequest(http.MethodPost, url, reader)
	if err != nil {
		return "", err
	}
	//Create an HTTP Client to set Request Headers
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("cache-control", "no-cache¬")
	client := &http.Client{Timeout: 10 * time.Second}
	res, err := client.Do(req)
	if res != nil {
		defer res.Body.Close()
	}
	if err != nil {
		return "", fmt.Errorf("could not send auth request: %v", err)
	}

	var authResponse AuthRes
	err = json.NewDecoder(res.Body).Decode(&authResponse)
	if err != nil {
		return "", fmt.Errorf("could not decode auth response: %v", err)
	}

	accessToken := authResponse.Key
	return accessToken, nil
}

func (s Service) NairobiBalance(verifyReq VerifyWaterBill) (string, error) {

	key, err := s.auth()
	if err != nil {
		return "", nil
	}

	verify := Balance{}
	verify.Username = s.Username
	verify.Key = key
	verify.AccountNumber = verifyReq.AccountNumber

	body, err := json.Marshal(verify)
	if err != nil {
		return "", nil
	}

	headers := make(map[string]string)
	headers["content-type"] = "application/json"
	headers["cache-control"] = "no-cache"

	url := s.baseURL() + "nairobiwateraccount"
	return s.newReq(url, body, headers)
}

func (s Service) KisumuBalance(verifyReq VerifyWaterBill) (string, error) {

	key, err := s.auth()
	if err != nil {
		return "", nil
	}

	verify := Balance{}
	verify.Username = s.Username
	verify.Key = key
	verify.AccountNumber = verifyReq.AccountNumber

	body, err := json.Marshal(verify)
	if err != nil {
		return "", nil
	}

	headers := make(map[string]string)
	headers["content-type"] = "application/json"
	headers["cache-control"] = "no-cache"

	url := s.baseURL() + "kisumuwateraccount"
	return s.newReq(url, body, headers)
}

// newReq handles all POST requests
func (s Service) newReq(url string, body []byte, headers map[string]string) (string, error) {
	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	if err != nil {
		return "", nil
	}

	for key, value := range headers {
		request.Header.Set(key, value)
	}

	client := &http.Client{Timeout: 60 * time.Second}
	res, err := client.Do(request)
	if res != nil {
		defer res.Body.Close()
	}
	if err != nil {
		return "", err
	}

	stringBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	return string(stringBody), nil
}

// baseURL determines URL depending with Env¬
func (s Service) baseURL() string {
	if s.Env == PRODUCTION {
		return "https://cellpointapp.com:2053/v3/"
	}
	return "https://test.cellpointapp.com:2053/v3/"
}
