package users

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"github.com/julienschmidt/httprouter"
	"github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
	"mwangaza/model"
	"mwangaza/notification"
	"mwangaza/settings"
	"net/http"
	"strings"
)

// Handler is a route routes for requests
type Handler struct {
	database *gorm.DB
}

// New creates a new Handler
func New(database *gorm.DB) Handler {
	return Handler{database: database}
}

// ---------------------------------------------------------------------------------------------------------------------
// USER API's
// ---------------------------------------------------------------------------------------------------------------------

// VerifyUser This is responsible for verifying if a user exists
func (h Handler) VerifyUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var user model.VerifyUser
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	res := h.database.Where(&model.User{PhoneNumber: user.PhoneNumber}).First(&model.User{})

	resp := res.Value
	actual := resp.(*model.User)

	if len(actual.PhoneNumber) > 0 {
		resp := new(model.Response)
		resp.Message = "User is Registered"

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(resp)

	} else {
		resp := new(model.Response)
		resp.Message = "User Not Registered"

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		_ = json.NewEncoder(w).Encode(resp)
	}
}

// RegisterUser This is responsible for registering a user
func (h Handler) RegisterUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var user model.RegisterUser
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	id := uuid.NewV4()

	hash, err := HashPassword(user.Pin)
	var phoneNumber string
	if strings.HasPrefix(user.PhoneNumber, "07") {
		phoneNumber = "254" + user.PhoneNumber[1:]
	} else {
		phoneNumber = user.PhoneNumber
	}

	res := h.database.Create(&model.User{UserId: id, Name: user.Name, PhoneNumber: phoneNumber,
		Pin: hash, FacebookToken: user.FacebookToken}).Scan(&model.User{})

	resp := res.Value
	actual := resp.(*model.User)

	if actual.Mwangaza.Active == true {

		//Create Wallet with User ID
		h.createUserWallet(id)

		var userAccount = model.UserAccount{}
		userAccount.UserId = actual.UserId
		userAccount.Name = actual.Name
		userAccount.PhoneNumber = actual.PhoneNumber
		userAccount.DeviceId = actual.DeviceId
		userAccount.FacebookToken = actual.FacebookToken
		userAccount.Active = actual.Mwangaza.Active
		userAccount.JwtToken.Token = createToken(actual.UserId)

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(userAccount)

	} else {
		resp := new(model.Response)
		resp.Message = "User already registered"

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		_ = json.NewEncoder(w).Encode(resp)
	}

	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

}

func (h Handler) createUserWallet(userId uuid.UUID) {

	id := uuid.NewV4()
	_ = h.database.Create(&model.Wallet{WalletId: id, UserId: userId, Balance: 0}).Scan(&model.Wallet{})

}

// LoginUser This is responsible for logging in a user
func (h Handler) LoginUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var user model.LoginUser
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	var phoneNumber string
	if strings.HasPrefix(user.PhoneNumber, "07") {
		phoneNumber = "254" + user.PhoneNumber[1:]
	} else {
		phoneNumber = user.PhoneNumber
	}

	res := h.database.Where(&model.User{PhoneNumber: phoneNumber}).First(&model.User{})

	resp := res.Value
	actual := resp.(*model.User)

	if len(actual.PhoneNumber) > 0 {
		match := CheckPasswordHash(user.Pin, actual.Pin)

		if match == true {

			var userAccount = model.UserAccount{}
			userAccount.UserId = actual.UserId
			userAccount.Name = actual.Name
			userAccount.PhoneNumber = actual.PhoneNumber
			userAccount.DeviceId = actual.DeviceId
			userAccount.FacebookToken = actual.FacebookToken
			userAccount.Active = actual.Mwangaza.Active
			userAccount.JwtToken.Token = createToken(actual.UserId)

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			_ = json.NewEncoder(w).Encode(userAccount)
		} else {
			resp := new(model.Response)
			resp.Message = "Wrong Password"

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusUnauthorized)
			_ = json.NewEncoder(w).Encode(resp)
		}
	} else {
		resp := new(model.Response)
		resp.Message = "User Not Found"

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		_ = json.NewEncoder(w).Encode(resp)
	}

	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func createToken(userId uuid.UUID) string {

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"userId": userId,
	})

	tokenString, err := token.SignedString([]byte(settings.GetSecret()))
	if err != nil {
		fmt.Println(err)
	}

	return tokenString
}

func (h Handler) SetFCMToken(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				_ = json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				var firebase model.Firebase
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&firebase)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				claims, _ := token.Claims.(jwt.MapClaims)

				res := h.database.Model(&model.User{}).Where("user_id =?", claims["userId"]).UpdateColumn("device_id", firebase.FirebaseToken)

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				_ = json.NewEncoder(w).Encode(res.Value)

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				_ = json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		_ = json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func (h Handler) UpdateUserInfo(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				_ = json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				var user model.UserData
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&user)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				claims, _ := token.Claims.(jwt.MapClaims)

				res := h.database.Model(&model.User{}).Where("user_id =?", claims["userId"]).UpdateColumn("name", user.Name)

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				_ = json.NewEncoder(w).Encode(res.Value)

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				_ = json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		_ = json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}
}

func (h Handler) ResetPin(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var user model.RequestOtp
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	var phoneNumber string
	if strings.Contains(user.PhoneNumber, "+") {
		str := strings.Split(user.PhoneNumber, "+")
		phoneNumber = str[1]
	} else {
		phoneNumber = user.PhoneNumber
	}

	res := h.database.Where(&model.User{PhoneNumber: phoneNumber}).First(&model.User{})

	resp := res.Value
	actual := resp.(*model.User)

	if len(actual.PhoneNumber) > 0 {

		//Send SMS OTP
		notification.New(h.database).SendOTP(actual.UserId, actual.Name, phoneNumber)

		var userReset = model.UserReset{}
		userReset.UserId = actual.UserId
		userReset.PhoneNumber = actual.PhoneNumber

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(userReset)

	} else {
		resp := new(model.Response)
		resp.Message = "User Not Found"

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		_ = json.NewEncoder(w).Encode(resp)
	}
}

func (h Handler) VerifyOTP(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var verify model.VerifyOtp
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&verify)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	user := model.Otp{}

	_ = h.database.Raw("SELECT * FROM otps WHERE user_id = ?", verify.UserId).Scan(&user)

	if user.Active == false {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		_ = json.NewEncoder(w).Encode(model.Response{Message: "OTP Code has already been used"})
	} else {
		if user.Otp == verify.Otp {
			_ = h.database.Model(&model.Otp{}).Where("user_id =?", verify.UserId).Update("active", false)

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			_ = json.NewEncoder(w).Encode(model.Response{Message: "User has been Verified successfully!"})

		} else {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusUnauthorized)
			_ = json.NewEncoder(w).Encode(model.Response{Message: "Wrong OTP Code"})
		}
	}
}

func (h Handler) UpdatePIN(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var user model.UpdateOtp
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	hash, err := HashPassword(user.Pin)

	res := h.database.Model(&model.User{}).Where("user_id =?", user.UserId).UpdateColumn("pin", hash)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(res.Value)

}
