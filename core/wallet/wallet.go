package wallet

import (
	"encoding/json"
	"fmt"
	"github.com/AndroidStudyOpenSource/mpesa-api-go"
	"github.com/davecgh/go-spew/spew"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"github.com/julienschmidt/httprouter"
	"github.com/negah/percent"
	"github.com/satori/go.uuid"
	"log"
	"math"
	"mwangaza/core/trans"
	"mwangaza/model"
	"mwangaza/notification"
	"mwangaza/settings"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// Handler is a route routes for requests
type Handler struct {
	database *gorm.DB
	mpesaB2C mpesa.Service
}

// New creates a new Handler
func New(database *gorm.DB, mpesaB2C mpesa.Service) Handler {
	return Handler{database: database, mpesaB2C: mpesaB2C}
}

// ---------------------------------------------------------------------------------------------------------------------
// WALLET BALANCE, TRANSFERS, TRANSACTIONS & DEPOSITS
// ---------------------------------------------------------------------------------------------------------------------

func (h Handler) WalletBalance(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)

				account := model.Account{}

				_ = h.database.Raw("SELECT a.balance, b.user_id FROM wallets a JOIN users b ON a.user_id = b.user_id WHERE a.user_id = ?", claims["userId"]).Scan(&account)

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				json.NewEncoder(w).Encode(account)

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}
}

func (h Handler) ShopBalance(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)

				account := model.ShopWalletBalance{}

				_ = h.database.Raw("SELECT b.user_id as shop_id, a.balance, a.commission FROM shop_wallets a JOIN users b ON a.shop_id = b.user_id WHERE a.shop_id = ?", claims["userId"]).Scan(&account)

				type ShopWalletBalance struct {
					ShopId     string `json:"shop_id"`
					Balance    string `json:"balance"`
					Commission string `json:"commission"`
				}

				var shopBalance = ShopWalletBalance{
					account.ShopId,
					strconv.Itoa(account.Balance),
					strconv.Itoa(account.Commission),
				}

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				json.NewEncoder(w).Encode(shopBalance)

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}
}

func (h Handler) CommissionToBalance(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)

				account := model.ShopWalletBalance{}
				accounts := model.ShopWalletBalance{}

				_ = h.database.Raw("SELECT b.user_id as shop_id, a.balance, a.commission FROM shop_wallets a JOIN users b ON a.shop_id = b.user_id WHERE a.shop_id = ?", claims["userId"]).Scan(&account)

				if account.Balance > 0 {
					_ = h.database.Model(&model.ShopWallet{}).Where("shop_id =?", claims["userId"]).UpdateColumn("balance", gorm.Expr("balance + ?", account.Commission))
					_ = h.database.Model(&model.ShopWallet{}).Where("shop_id =?", claims["userId"]).UpdateColumn("commission", gorm.Expr("commission - ?", account.Commission))
					_ = h.database.Raw("SELECT b.user_id as shop_id, a.balance, a.commission FROM shop_wallets a JOIN users b ON a.shop_id = b.user_id WHERE a.shop_id = ?", claims["userId"]).Scan(&accounts)

					type ShopWalletBalance struct {
						ShopId     string `json:"shop_id"`
						Balance    string `json:"balance"`
						Commission string `json:"commission"`
					}

					var shopBalance = ShopWalletBalance{
						accounts.ShopId,
						strconv.Itoa(accounts.Balance),
						strconv.Itoa(accounts.Commission),
					}

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusOK)
					json.NewEncoder(w).Encode(shopBalance)

				} else {

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusOK)
					json.NewEncoder(w).Encode(account)

				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}
}

func (h Handler) Transactions(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)

				rows, _ := h.database.Limit(10).Raw("SELECT transaction_id, name, amount FROM transactions WHERE user_id = ? ORDER BY created_at DESC", claims["userId"]).Rows()
				var transRes []model.TransactionRes
				for rows.Next() {
					var tran model.TransactionRes
					h.database.ScanRows(rows, &tran)
					transRes = append(transRes, tran)
				}

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				json.NewEncoder(w).Encode(transRes)

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}
}

func (h Handler) WalletDeposits(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)

				account := model.User{}

				_ = h.database.Raw("SELECT * FROM users WHERE user_id = ? ORDER BY created_at DESC", claims["userId"]).Scan(&account)

				rows, _ := h.database.Raw("SELECT mpesa_transaction_id, trans_id, trans_time, trans_amount FROM mpesa_transactions WHERE msisdn = ?", account.PhoneNumber).Rows()
				var deposits []model.WalletDeposits
				for rows.Next() {
					var deposit model.WalletDeposits
					h.database.ScanRows(rows, &deposit)
					deposits = append(deposits, deposit)
				}

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				json.NewEncoder(w).Encode(deposits)

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}
}

func (h Handler) FetchTransactions(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

}

func (h Handler) SendMoney(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}

			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)
				wallet := model.Wallet{}

				var payload model.Payment
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&payload)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				phones := strings.Replace(payload.AccountNumber, " ", "", -1)
				fmt.Println("The Phones Number is ", phones)

				var phoneNumber string
				if strings.HasPrefix(phones, "07") {
					phoneNumber = "254" + phones[1:]
				} else if strings.Contains(phones, "+") {
					str := strings.Split(phones, "+")
					phoneNumber = str[1]
				} else {
					phoneNumber = phones
				}

				fmt.Println("The Phones is ", phoneNumber)

				_ = h.database.Raw("SELECT * FROM wallets WHERE user_id = ?", claims["userId"]).Scan(&wallet)

				if !wallet.Mwangaza.Active {
					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "User not found"})
					return
				}

				tarrif := h.calculateB2CTarrif(int(payload.Amount))

				amount := int(payload.Amount) + tarrif

				if amount <= wallet.Balance {
					id := uuid.NewV4()

					paymentRes := model.MpesaB2cPayments{}

					_ = h.database.Create(&model.MpesaB2cPayments{BusinessCustomerPaymentId: id, UserId: payload.UserId, Amount: strconv.Itoa(int(payload.Amount)),
						AccountNumber: phoneNumber}).Scan(&paymentRes)

					_ = h.database.Model(&model.Wallet{}).Where("user_id =?", claims["userId"]).UpdateColumn("balance", gorm.Expr("balance - ?", amount))

					h.makeB2CPayments(paymentRes)

					trans.New(h.database).CreateTransactionRecord(wallet.UserId, "Send Money", int(payload.Amount))
					_ = h.database.Create(&model.SendMoneyTariff{SendMoneyTariffId: id, UserId: wallet.UserId, ShareMoneyId: paymentRes.BusinessCustomerPaymentId, Amount: tarrif})

					resp := new(model.TransactionResponse)
					resp.TransRef = paymentRes.BusinessCustomerPaymentId
					resp.Message = "Send Money Request has been submitted successfully"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusOK)
					json.NewEncoder(w).Encode(resp)

				} else {
					resp := new(model.Response)
					resp.Message = "Your Wallet Balance is Insufficient"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(resp)
				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func (h Handler) SendMoneyShop(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}

			resp := new(model.Response)
			resp.Message = "Try again later"

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusUnauthorized)
			json.NewEncoder(w).Encode(resp)

			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)
				wallet := model.ShopWallet{}

				var payload model.Payment
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&payload)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				phones := strings.Replace(payload.AccountNumber, " ", "", -1)
				fmt.Println("The Phones Number is ", phones)

				var phoneNumber string
				if strings.HasPrefix(phones, "07") {
					phoneNumber = "254" + phones[1:]
				} else if strings.Contains(phones, "+") {
					str := strings.Split(phones, "+")
					phoneNumber = str[1]
				} else {
					phoneNumber = phones
				}

				fmt.Println("The Phones is ", phoneNumber)

				_ = h.database.Raw("SELECT * FROM shop_wallets WHERE shop_id = ?", claims["userId"]).Scan(&wallet)

				if !wallet.Mwangaza.Active {
					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(model.Response{Message: "User not found"})
					return
				}

				tarrif := h.calculateB2CTarrif(int(payload.Amount))

				amount := int(payload.Amount) + tarrif

				if amount <= wallet.Balance {
					id := uuid.NewV4()

					paymentRes := model.MpesaB2cPayments{}

					_ = h.database.Create(&model.MpesaB2cPayments{BusinessCustomerPaymentId: id, UserId: payload.UserId, Amount: strconv.Itoa(int(payload.Amount)),
						AccountNumber: phoneNumber}).Scan(&paymentRes)

					_ = h.database.Model(&model.ShopWallet{}).Where("shop_id =?", claims["userId"]).UpdateColumn("balance", gorm.Expr("balance - ?", amount))

					h.makeB2CPayments(paymentRes)

					trans.New(h.database).CreateTransactionRecord(wallet.ShopId, "Send Money", int(payload.Amount))
					_ = h.database.Create(&model.SendMoneyTariff{SendMoneyTariffId: id, UserId: wallet.ShopId, ShareMoneyId: paymentRes.BusinessCustomerPaymentId, Amount: tarrif})

					resp := new(model.TransactionResponse)
					resp.TransRef = paymentRes.BusinessCustomerPaymentId
					resp.Message = "Send Money Request has been submitted successfully"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusOK)
					json.NewEncoder(w).Encode(resp)

				} else {
					resp := new(model.Response)
					resp.Message = "Your Wallet Balance is Insufficient"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(resp)
				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func (h Handler) PayBill(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}

			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)
				wallet := model.Wallet{}

				var payload model.Paybill
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&payload)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				if payload.Paybill == "888880" {

					resp := new(model.Response)
					resp.Message = "Buy Tokens using the Kenya Power option on the Menu"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(resp)
				} else if payload.Paybill == "700205" {
					resp := new(model.Response)
					resp.Message = "Kindly try back later"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(resp)
				} else {

					_ = h.database.Raw("SELECT * FROM wallets WHERE user_id = ?", claims["userId"]).Scan(&wallet)

					if !wallet.Mwangaza.Active {
						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "User not found"})
						return
					}

					tarrif := h.calculateB2BTarrif(int(payload.Amount))

					amount := int(payload.Amount) + tarrif

					if amount <= wallet.Balance {
						id := uuid.NewV4()

						paymentRes := model.MpesaB2bPayments{}

						_ = h.database.Create(&model.MpesaB2bPayments{BusinessBusinessPaymentId: id, UserId: payload.UserId, Amount: strconv.Itoa(int(payload.Amount)),
							AccountNumber: payload.AccountNumber, PayBill: payload.Paybill}).Scan(&paymentRes)

						_ = h.database.Model(&model.Wallet{}).Where("user_id =?", claims["userId"]).UpdateColumn("balance", gorm.Expr("balance - ?", amount))

						str := spew.Sdump(paymentRes)
						fmt.Println(str)

						h.makeB2BPayments(paymentRes)
						trans.New(h.database).CreateTransactionRecord(wallet.UserId, "Pay Bill", int(payload.Amount))
						_ = h.database.Create(&model.PaybillTariff{PaybillTariffId: id, UserId: wallet.UserId, PaybillId: paymentRes.BusinessBusinessPaymentId, Amount: tarrif})

						resp := new(model.TransactionResponse)
						resp.TransRef = paymentRes.BusinessBusinessPaymentId
						resp.Message = "Send Money Request has been submitted successfully"

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(resp)

					} else {
						resp := new(model.Response)
						resp.Message = "Your Wallet Balance is Insufficient"

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(resp)
					}
				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func (h Handler) ShopPayBill(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}

			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)
				wallet := model.ShopWallet{}

				var payload model.Paybill
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&payload)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				if payload.Paybill == "888880" {

					resp := new(model.Response)
					resp.Message = "Buy Tokens using the Kenya Power option on the Menu"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(resp)
				} else {
					_ = h.database.Raw("SELECT * FROM shop_wallets WHERE shop_id = ?", claims["userId"]).Scan(&wallet)

					if !wallet.Mwangaza.Active {
						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(model.Response{Message: "User not found"})
						return
					}

					tarrif := h.calculateB2BTarrif(int(payload.Amount))
					bonus := getPercentage(tarrif)

					amount := int(payload.Amount) + tarrif

					if amount <= wallet.Balance {
						id := uuid.NewV4()

						paymentRes := model.MpesaB2bPayments{}

						_ = h.database.Create(&model.MpesaB2bPayments{BusinessBusinessPaymentId: id, UserId: payload.UserId, Amount: strconv.Itoa(int(payload.Amount)),
							AccountNumber: payload.AccountNumber, PayBill: payload.Paybill}).Scan(&paymentRes)

						_ = h.database.Model(&model.ShopWallet{}).Where("shop_id =?", claims["userId"]).UpdateColumn("balance", gorm.Expr("balance - ?", amount))

						h.makeB2BPayments(paymentRes)

						trans.New(h.database).AddTransactionBonus(wallet.ShopId, bonus)
						trans.New(h.database).CreateShopTransactionRecord(wallet.ShopId, "Pay Bill", int(payload.Amount), bonus)
						_ = h.database.Create(&model.PaybillTariff{PaybillTariffId: id, UserId: wallet.ShopId, PaybillId: paymentRes.BusinessBusinessPaymentId, Amount: tarrif})

						resp := new(model.TransactionResponse)
						resp.TransRef = paymentRes.BusinessBusinessPaymentId
						resp.Message = "Send Money Request has been submitted successfully"

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(resp)

					} else {
						resp := new(model.Response)
						resp.Message = "Your Wallet Balance is Insufficient"

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(resp)
					}
				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}
}

func (h Handler) ShareFunds(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				var shareFunds model.ShareFunds
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&shareFunds)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				var phoneNumber string
				if strings.HasPrefix(shareFunds.PhoneNumber, "07") {
					phoneNumber = "254" + shareFunds.PhoneNumber[1:]
				} else {
					phoneNumber = shareFunds.PhoneNumber
				}

				claims, _ := token.Claims.(jwt.MapClaims)

				account := model.User{}

				_ = h.database.Raw("SELECT * FROM users WHERE phone_number = ?", phoneNumber).Scan(&account)

				if len(account.PhoneNumber) > 0 {

					wallet := model.Wallet{}

					_ = h.database.Raw("SELECT * FROM wallets WHERE user_id = ?", claims["userId"]).Scan(&wallet)

					if int(shareFunds.Amount) <= wallet.Balance {

						var shareRes model.Wallet

						_ = h.database.Model(&model.Wallet{}).Where("user_id =?", claims["userId"]).UpdateColumn("balance", gorm.Expr("balance - ?", shareFunds.Amount)).Scan(&shareRes)

						_ = h.database.Model(&model.Wallet{}).Where("user_id =?", account.UserId).UpdateColumn("balance", gorm.Expr("balance + ?", shareFunds.Amount))

						trans.New(h.database).CreateTransactionRecord(wallet.UserId, "Share Funds", int(shareFunds.Amount))

						var shareFundsRes = model.ShareFundsRes{}
						shareFundsRes.Name = account.Name
						shareFundsRes.PhoneNumber = account.PhoneNumber
						shareFundsRes.Amount = int(shareFunds.Amount)
						shareFundsRes.Balance = shareRes.Balance

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusOK)
						json.NewEncoder(w).Encode(shareFundsRes)

					} else {
						resp := new(model.Response)
						resp.Message = "Your Wallet Balance is Insufficient"

						w.Header().Set("Content-Type", "application/json")
						w.WriteHeader(http.StatusUnauthorized)
						json.NewEncoder(w).Encode(resp)
					}

				} else {
					resp := new(model.Response)
					resp.Message = "User Is Not Registered"

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(resp)
				}

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}
}

// ---------------------------------------------------------------------------------------------------------------------
// MPESA C2B API
// ---------------------------------------------------------------------------------------------------------------------

// UpdateTopUps This saves all transactions pushed from MPESA
func (h Handler) UpdatePull(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var payload model.C2BPull
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&payload)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	if payload.ResponseCode == "1000" {

		for _, s := range payload.Response {
			for _, d := range s {

				id := uuid.NewV4()
				if err != nil {
					fmt.Printf("Something went wrong: %s", err)
					return
				}

				var phoneNumber string
				if strings.HasPrefix(d.Msisdn, "07") {
					phoneNumber = "254" + d.Msisdn[1:]
				} else {
					phoneNumber = d.Msisdn
				}

				res := h.database.Create(&model.MpesaPull{MpesaPullId: id, TransID: d.TransactionID, BillReference: d.Billreference,
					TransTime: d.TrxDate, TransAmount: strconv.Itoa(d.Amount), MSISDN: phoneNumber})

				if err != nil {
					http.Error(w, "Internal Server Error", http.StatusInternalServerError)
					return
				}

				h.reconcileWalletBalance(res.Value)
			}
		}
	}
}

func (h Handler) ReconcileWallet(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var payload model.MpesaReconcile
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&payload)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	str := spew.Sdump(payload)
	fmt.Print(str)

	id := uuid.NewV4()

	var phoneNumber string
	if strings.HasPrefix(payload.MSISDN, "07") {
		phoneNumber = "254" + payload.MSISDN[1:]
	} else {
		phoneNumber = payload.MSISDN
	}

	res := h.database.Create(&model.MpesaReconcile{MpesaReconcileId: id, TransID: payload.TransID, BillReference: payload.BillReference,
		TransTime: payload.TransTime, TransAmount: payload.TransAmount, MSISDN: phoneNumber})

	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	h.reconcileWalletBalances(res.Value)

}

// reconcileWalletBalance updates the user's account balance
func (h Handler) reconcileWalletBalances(resp interface{}) {

	transaction := resp.(*model.MpesaReconcile)

	if len(transaction.MSISDN) > 0 {
		res := h.database.Where(&model.User{PhoneNumber: transaction.MSISDN}).First(&model.User{})

		user := res.Value
		actual := user.(*model.User)

		// Look for the transaction code on the transaction table
		isCode := h.database.Where(&model.MpesaTransaction{TransID: transaction.TransID}).First(&model.MpesaTransaction{})
		mpesaTransaction := isCode.Value
		actualTransaction := mpesaTransaction.(*model.MpesaTransaction)

		fmt.Println("Juma ", actualTransaction.TransID)
		fmt.Println("Juma ", len(actualTransaction.TransID))

		if len(actualTransaction.TransID) > 0 {
			fmt.Println("Transaction already exists, skip the record...")
		} else {

			fmt.Println("Transaction does not exist")

			if actual.Shop == true {

				_ = h.database.Model(&model.ShopWallet{}).Where("shop_id =?", actual.UserId).UpdateColumn("balance", gorm.Expr("balance + ?", transaction.TransAmount))

				msg := "Hi. Your wallet deposit of KES " + transaction.TransAmount + " was successful"
				fmt.Println(msg)
				notification.New(h.database).SendPushNotification(msg, "", actual.DeviceId)
			} else {
				_ = h.database.Model(&model.Wallet{}).Where("user_id =?", actual.UserId).UpdateColumn("balance", gorm.Expr("balance + ?", transaction.TransAmount))

				msg := "Hi. Your wallet deposit of KES " + transaction.TransAmount + " was successful"
				fmt.Println(msg)
				notification.New(h.database).SendPushNotification(msg, "", actual.DeviceId)
			}
		}

	} else {
		fmt.Println("Mwangaza Withdrawal Callback....")
	}
}

func (h Handler) reconcileWalletBalance(resp interface{}) {

	transaction := resp.(*model.MpesaPull)

	if len(transaction.MSISDN) > 0 {
		res := h.database.Where(&model.User{PhoneNumber: transaction.MSISDN}).First(&model.User{})

		user := res.Value
		actual := user.(*model.User)

		// Look for the transaction code on the transaction table
		isCode := h.database.Where(&model.MpesaTransaction{TransID: transaction.TransID}).First(&model.MpesaTransaction{})
		mpesaTransaction := isCode.Value
		actualTransaction := mpesaTransaction.(*model.MpesaTransaction)

		fmt.Println("Juma ", actualTransaction.TransID)
		fmt.Println("Juma ", len(actualTransaction.TransID))

		if len(actualTransaction.TransID) > 0 {
			fmt.Println("Transaction already exists, skip the record...")
		} else {

			fmt.Println("Transaction does not exist")

			if actual.Shop == true {

				_ = h.database.Model(&model.ShopWallet{}).Where("shop_id =?", actual.UserId).UpdateColumn("balance", gorm.Expr("balance + ?", transaction.TransAmount))

				msg := "Hi. Your wallet deposit of KES " + transaction.TransAmount + " was successful"
				fmt.Println(msg)
				notification.New(h.database).SendPushNotification(msg, "", actual.DeviceId)
			} else {
				_ = h.database.Model(&model.Wallet{}).Where("user_id =?", actual.UserId).UpdateColumn("balance", gorm.Expr("balance + ?", transaction.TransAmount))

				msg := "Hi. Your wallet deposit of KES " + transaction.TransAmount + " was successful"
				fmt.Println(msg)
				notification.New(h.database).SendPushNotification(msg, "", actual.DeviceId)
			}
		}

	} else {
		fmt.Println("Mwangaza Withdrawal Callback....")
	}
}

// UpdateTopUps This saves all transactions pushed from MPESA
func (h Handler) UpdateTopUps(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var payload model.C2BCallBack
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&payload)

	fmt.Println(err)

	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
	}

	str := spew.Sdump(payload)
	log.Println(str)

	id := uuid.NewV4()
	if err != nil {
		fmt.Printf("Something went wrong: %s", err)
	}

	if len(payload.MSISDN) > 0 {
		amount := strings.Split(payload.TransAmount, ".")
		balance := strings.Split(payload.OrgAccountBalance, ".")

		var phoneNumber string
		if strings.HasPrefix(payload.MSISDN, "07") {
			phoneNumber = "254" + payload.MSISDN[1:]
		} else {
			phoneNumber = payload.MSISDN
		}

		res := h.database.Create(&model.MpesaTransaction{MpesaTransactionId: id, TransID: payload.TransID, TransTime: payload.TransTime,
			TransAmount: amount[0], BusinessShortCode: payload.BusinessShortCode, OrgAccountBalance: balance[0],
			MSISDN: phoneNumber, FirstName: payload.FirstName, MiddleName: payload.MiddleName, LastName: payload.LastName})

		if err != nil {
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		h.updateWalletBalance(res.Value)
	}
}

// updateWalletBalance updates the user's account balance
func (h Handler) updateWalletBalance(resp interface{}) {

	transaction := resp.(*model.MpesaTransaction)

	if len(transaction.MSISDN) > 0 {
		res := h.database.Where(&model.User{PhoneNumber: transaction.MSISDN}).First(&model.User{})

		user := res.Value
		actual := user.(*model.User)

		if actual.Shop == true {

			_ = h.database.Model(&model.ShopWallet{}).Where("shop_id =?", actual.UserId).UpdateColumn("balance", gorm.Expr("balance + ?", transaction.TransAmount))

			msg := "Hi. Your wallet deposit of KES " + transaction.TransAmount + " was successful"
			notification.New(h.database).SendPushNotification(msg, "", actual.DeviceId)
		} else {
			_ = h.database.Model(&model.Wallet{}).Where("user_id =?", actual.UserId).UpdateColumn("balance", gorm.Expr("balance + ?", transaction.TransAmount))

			msg := "Hi. Your wallet deposit of KES " + transaction.TransAmount + " was successful"
			notification.New(h.database).SendPushNotification(msg, "", actual.DeviceId)
		}

	} else {
		fmt.Println("Mwangaza Withdrawal Callback....")
	}
}

// call the device with updated wallet balance
func (h Handler) pingDevice() {

}

func (h Handler) makeB2CPayments(payment model.MpesaB2cPayments) {

	//Make B2C Request to M-Pesa
	resp, err := h.mpesaB2C.B2CRequest(mpesa.B2C{
		InitiatorName:      "B2cMwangazaInitiator",
		SecurityCredential: "TdTyECIjYFA9gAlohe6WZTmK3nS14oYgd6Nmb4leuV+DRa18eePr+fQDLoru+ocGgjuHOzrr3vJPxlFtgb0n8ohCelNqEJLq2K/zWA7wDzjoVM/MDD3KETst07HRXY01QLui3eFJlHrzYbRTqDJ7galDCUD8cpnvdL5qVtTBRqrYsgJ7jPk1XfKH7ZQT/QwoUPwuoE9a9TUAcvSiJj2EU/LwMmTKP4iggMCtcG+BcG+SJ1Xsgp5X/TYJcVGOR87RmaBWymY8UqcNgImVMwL/NJ+F6O0rJeAZGKOEGn3Rd/myHLwv5DpoS5qeAOoMak8VVqJ+9yQBXMJ6Rhh1L7m/xQ==",
		Remarks:            "demo",
		CommandID:          "BusinessPayment",
		Amount:             payment.Amount,
		PartyA:             "694724",
		PartyB:             payment.AccountNumber,
		QueueTimeOutURL:    "https://api.mwangaza.ke/api/v1/b2c/timeout",
		ResultURL:          "https://api.mwangaza.ke/api/v1/b2c/result",
		Occassion:          "mwangaza",
	})

	if err != nil {
		log.Println(err)
	}

	//Decode Safaricom Response
	var res model.MPESAResponse
	err = json.Unmarshal([]byte(resp), &res)
	if err != nil {
		log.Println(err)
	}
	// Update B2C Request Response
	err = h.database.Model(&model.MpesaB2cPayments{}).Where("business_customer_payment_id = ?", payment.BusinessCustomerPaymentId).
		Updates(model.MpesaB2cPayments{ConversationId: res.ConversationID, OriginatorConversationId: res.OriginatorConversationID}).Error

	if err != nil {
		log.Println(err)
	}
}

func (h Handler) B2CResult(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var b2cCallback model.B2CCallBack
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&b2cCallback)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	str := spew.Sdump(b2cCallback)
	fmt.Println(str)

	result := b2cCallback.Result
	var ReceiverPartyPublicName string
	var B2CRecipientIsRegisteredCustomer string
	var TransactionCompletedDateTime string

	for _, param := range result.ResultParameters.ResultParameter {
		key := param.Key
		if key == "ReceiverPartyPublicName" {
			value := param.Value
			if value != nil {
				ReceiverPartyPublicName = value.(string)
			}
		} else if key == "TransactionCompletedDateTime" {
			value := param.Value
			if value != nil {
				TransactionCompletedDateTime = value.(string)
			}
		} else if key == "B2CRecipientIsRegisteredCustomer" {
			value := param.Value
			if value != nil {
				B2CRecipientIsRegisteredCustomer = value.(string)
			}
		}
	}

	if ReceiverPartyPublicName == "" || B2CRecipientIsRegisteredCustomer == "" || TransactionCompletedDateTime == "" {
		fmt.Println("params are null")
	}

	//ReceiverName Struct
	type ReceiverName struct {
		ReceiverName string
	}

	//Split the tow, and trim spaces
	receiverName := new(ReceiverName)
	parts := strings.Split(ReceiverPartyPublicName, "-")
	name := strings.TrimSpace(parts[1])
	receiverName.ReceiverName = name

	metadata, err := json.Marshal(result)
	if err != nil {
		fmt.Println(err)
	}

	// Update AGAIN (2) B2C Request Response
	res := h.database.Model(&model.MpesaB2cPayments{}).Where("conversation_id = ?", result.ConversationID).
		Updates(model.MpesaB2cPayments{TransactionReceipt: result.TransactionID, ReceiverPartyPublicName: receiverName.ReceiverName,
			B2cRecipientIsRegisteredCustomer: B2CRecipientIsRegisteredCustomer, TransactionCompletedDatetime: time.Now(), TransactionStatus: true,
			Metadata: string(metadata)}).First(&model.MpesaB2cPayments{})

	m := res.Value
	actual := m.(*model.MpesaB2cPayments)

	resp := h.database.Where(&model.User{UserId: actual.UserId}).First(&model.User{})

	stri := spew.Sdump(resp)
	fmt.Println(stri)

	user := resp.Value
	us := user.(*model.User)

	msg := "Hi. Your Send Money request of KES " + actual.Amount + " was successful"

	notification.New(h.database).SendPushNotification(msg, "", us.DeviceId)

	if err != nil {
		log.Println(err)
		return
	}

}

func (h Handler) B2CTimeout(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

}

func (h Handler) makeB2BPayments(payment model.MpesaB2bPayments) {

	//Make B2C Request to M-Pesa
	resp, err := h.mpesaB2C.B2BRequest(mpesa.B2B{
		Initiator:              "B2cMwangazaInitiator",
		SecurityCredential:     "TdTyECIjYFA9gAlohe6WZTmK3nS14oYgd6Nmb4leuV+DRa18eePr+fQDLoru+ocGgjuHOzrr3vJPxlFtgb0n8ohCelNqEJLq2K/zWA7wDzjoVM/MDD3KETst07HRXY01QLui3eFJlHrzYbRTqDJ7galDCUD8cpnvdL5qVtTBRqrYsgJ7jPk1XfKH7ZQT/QwoUPwuoE9a9TUAcvSiJj2EU/LwMmTKP4iggMCtcG+BcG+SJ1Xsgp5X/TYJcVGOR87RmaBWymY8UqcNgImVMwL/NJ+F6O0rJeAZGKOEGn3Rd/myHLwv5DpoS5qeAOoMak8VVqJ+9yQBXMJ6Rhh1L7m/xQ==",
		SenderIdentifierType:   "4",
		RecieverIdentifierType: "4",
		Remarks:                "demo",
		CommandID:              "BusinessPayBill",
		Amount:                 payment.Amount,
		PartyA:                 "694724",
		PartyB:                 payment.PayBill,
		AccountReference:       payment.AccountNumber,
		QueueTimeOutURL:        "https://api.mwangaza.ke/api/v1/b2b/timeout",
		ResultURL:              "https://api.mwangaza.ke/api/v1/b2b/result",
	})

	if err != nil {
		log.Println(err)
	}

	//Decode Safaricom Response
	var res model.MPESAResponse
	err = json.Unmarshal([]byte(resp), &res)
	if err != nil {
		log.Println(err)
	}

	str := spew.Sdump(res)
	fmt.Println(str)

	// Save B2B Request Response
	err = h.database.Model(&model.MpesaB2bPayments{}).Where("business_business_payment_id = ?", payment.BusinessBusinessPaymentId).
		Updates(model.MpesaB2bPayments{ConversationId: res.ConversationID, OriginatorConversationId: res.OriginatorConversationID}).Error

	if err != nil {
		log.Println(err)
	}
}

func (h Handler) B2BResult(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var b2bCallback model.B2BCallBack
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&b2bCallback)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	str := spew.Sdump(b2bCallback)
	fmt.Println(str)

	result := b2bCallback.Result
	var ReceiverPartyPublicName string
	var TransCompletedTime string

	for _, param := range result.ResultParameters.ResultParameter {
		key := param.Key
		if key == "ReceiverPartyPublicName" {
			value := param.Value
			if value != nil {
				ReceiverPartyPublicName = value.(string)
			}
		} else if key == "TransCompletedTime" {
			value := param.Value
			if value != nil {
				TransCompletedTime = strconv.FormatFloat(value.(float64), 'f', 2, 64)
			}
		}
	}

	if ReceiverPartyPublicName == "" || TransCompletedTime == "" {
		fmt.Println("params are null")
	}

	//BankName Struct
	type BankName struct {
		BankName string
	}

	//Split the two, and trim spaces
	bankName := new(BankName)
	parts := strings.Split(ReceiverPartyPublicName, "-")
	name := strings.TrimSpace(parts[1])
	bankName.BankName = name

	metadata, err := json.Marshal(b2bCallback)
	if err != nil {
		fmt.Println(err)
	}

	// Save B2C Request Response
	res := h.database.Model(&model.MpesaB2bPayments{}).Where("conversation_id = ?", result.ConversationID).
		Updates(model.MpesaB2bPayments{TransactionReceipt: result.TransactionID, BankName: bankName.BankName,
			TransactionCompletedDatetime: time.Now(), TransactionStatus: true,
			Metadata: string(metadata)}).First(&model.MpesaB2bPayments{})

	m := res.Value
	actual := m.(*model.MpesaB2bPayments)

	resp := h.database.Where(&model.User{UserId: actual.UserId}).First(&model.User{})

	user := resp.Value
	us := user.(*model.User)

	msg := "Hi. Your Payment request of KES " + actual.Amount + " was successful"

	notification.New(h.database).SendPushNotification(msg, "", us.DeviceId)

	if err != nil {
		log.Println(err)
		return
	}
}

func (h Handler) B2BTimeout(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

}

func (h Handler) XpressCallback(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	str := spew.Sdump(r.Body)
	fmt.Println(str)
}

func (h Handler) CalculateB2CTarrifs(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	res := h.calculateB2CTarrif(4990)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(res)
}

func (h Handler) calculateB2CTarrif(amount int) int {
	if amount <= 100 {
		return 21
	} else if amount >= 101 && amount <= 499 {
		return 25
	} else if amount >= 500 && amount <= 999 {
		return 30
	} else if amount >= 1000 && amount <= 1499 {
		return 36
	} else if amount >= 1500 && amount <= 2499 {
		return 41
	} else if amount >= 2500 && amount <= 3500 {
		return 46
	} else if amount >= 3501 && amount <= 5000 {
		return 53
	} else if amount >= 5001 && amount <= 7500 {
		return 66
	} else if amount >= 7501 && amount <= 10000 {
		return 78
	} else if amount >= 10001 && amount <= 15000 {
		return 103
	} else if amount >= 15001 && amount <= 20000 {
		return 128
	} else if amount >= 20001 && amount <= 70000 {
		return 233
	}
	return 0
}

func (h Handler) calculateB2BTarrif(amount int) int {
	if amount <= 100 {
		return 10
	} else if amount >= 101 && amount <= 500 {
		return 15
	} else if amount >= 501 && amount <= 1000 {
		return 20
	} else if amount >= 1001 && amount <= 1500 {
		return 25
	} else if amount >= 1501 && amount <= 2500 {
		return 30
	} else if amount >= 2501 && amount <= 3500 {
		return 35
	} else if amount >= 3501 && amount <= 5000 {
		return 50
	} else if amount >= 5001 && amount <= 7500 {
		return 60
	} else if amount >= 7501 && amount <= 10000 {
		return 60
	} else if amount >= 10001 && amount <= 15000 {
		return 85
	} else if amount >= 15001 && amount <= 35000 {
		return 100
	} else if amount >= 35001 && amount <= 50000 {
		return 190
	}
	return 190
}

func getPercentage(amount int) int {
	res := percent.Percent(30, amount)
	return int(math.Round(res))
}
