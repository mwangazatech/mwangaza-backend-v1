package trans

import (
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
	"mwangaza/model"
)

// Handler is a route routes for requests
type Handler struct {
	database *gorm.DB
}

// New creates a new Handler
func New(database *gorm.DB) Handler {
	return Handler{database: database}
}

func (h Handler) CreateTransactionRecord(userId uuid.UUID, transName string, amount int) {

	id := uuid.NewV4()
	h.database.Create(&model.Transaction{TransactionId: id, UserId: userId, Name: transName, Amount: amount})
}

func (h Handler) AddTransactionBonus(shopId uuid.UUID, amount int) {

	_ = h.database.Model(&model.ShopWallet{}).Where("shop_id =?", shopId).UpdateColumn("commission", gorm.Expr("commission + ?", amount))

}

func (h Handler) CreateShopTransactionRecord(shopId uuid.UUID, transName string, amount int, commission int) {

	id := uuid.NewV4()
	h.database.Create(&model.ShopTransaction{TransactionId: id, ShopId: shopId, Name: transName, Amount: amount, Commission: commission})
}
