package settings

import "os"

/**
	ENV ----------------------------------------------------------------------------------------------------------------
 */

// GetEnv is GetEnv
func GetEnv() string {
	env := os.Getenv("ENV")
	if env == "" {
		env = "DEMO"
	}
	return env
}

/**
	SECRET -------------------------------------------------------------------------------------------------------------
 */

// GetEnv is GetEnv
func GetSecret() string {
	env := os.Getenv("SECRET")
	if env == "" {
		env = "ilovegolang"
	}
	return env
}

/**
	DB -----------------------------------------------------------------------------------------------------------------
 */

// GetDBHost is GetDBHost
func GetDBHost() string {
	dbHost := os.Getenv("DB_HOST")
	if dbHost == "" {
		dbHost = "localhost"
	}
	return dbHost
}

// GetDBName is GetDBName
func GetDBName() string {
	dbName := os.Getenv("DB_NAME")
	if dbName == "" {
		dbName = "mwangaza"
	}
	return dbName
}

// GetDBUser is GetDBUser
func GetDBUser() string {
	dbUser := os.Getenv("DB_USER")
	if dbUser == "" {
		dbUser = "juma"
	}
	return dbUser
}

// GetDBPassword is GetDBPassword
func GetDBPassword() string {
	dbPassword := os.Getenv("DB_PASSWORD")
	if dbPassword == "" {
		dbPassword = "juma"
	}
	return dbPassword
}
