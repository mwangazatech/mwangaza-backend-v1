package app

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"github.com/julienschmidt/httprouter"
	"github.com/satori/go.uuid"
	"mwangaza/model"
	"mwangaza/settings"
	"net/http"
	"strings"
)

// Handler is a route routes for requests
type Handler struct {
	database *gorm.DB
}

// New creates a new Handler
func New(database *gorm.DB) Handler {
	return Handler{database: database}
}

// ---------------------------------------------------------------------------------------------------------------------
// APP VERSION CHECK API :: NOT SECURED [NO TOKEN REQUIRED]
// ---------------------------------------------------------------------------------------------------------------------

// CheckAppUpdate enables the app to check for the latest version
func (h Handler) CheckApiHealth(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	type Version struct {
		Status string `json:"status"`
	}

	version := new(Version)
	version.Status = "Mwangaza API is Healthy"

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(version)

}

func (h Handler) AppConfigs(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	type Version struct {
		Status string `json:"status"`
	}

	version := new(Version)
	version.Status = "Mwangaza API is Healthy"

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(version)

}

func (h Handler) Last5PhoneNumbers(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)

				rows, _ := h.database.Limit(5).Raw("SELECT DISTINCT phone_number FROM airtimes WHERE user_id = ? ORDER BY created_at DESC", claims["userId"]).Rows()
				var phoneRes []model.PhoneRes
				for rows.Next() {
					var phone model.PhoneRes
					h.database.ScanRows(rows, &phone)
					phoneRes = append(phoneRes, phone)
				}

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				json.NewEncoder(w).Encode(phoneRes)

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func (h Handler) Last5PrePaidMeterNumbers(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)

				rows, _ := h.database.Limit(5).Raw("SELECT DISTINCT meter_number FROM pre_paids WHERE user_id = ?", claims["userId"]).Rows()
				var ppaidRes []model.PrePaidRes
				for rows.Next() {
					var prepaid model.PrePaidRes
					h.database.ScanRows(rows, &prepaid)
					ppaidRes = append(ppaidRes, prepaid)
				}

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				json.NewEncoder(w).Encode(ppaidRes)

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func (h Handler) Last5PostPaidMeterNumbers(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)

				rows, _ := h.database.Limit(5).Raw("SELECT DISTINCT account_number FROM post_paids WHERE user_id = ?", claims["userId"]).Rows()
				var postpdRes []model.PostPaidRes
				for rows.Next() {
					var postpaid model.PostPaidRes
					h.database.ScanRows(rows, &postpaid)
					postpdRes = append(postpdRes, postpaid)
				}

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				json.NewEncoder(w).Encode(postpdRes)

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func (h Handler) Last5PostPaidWaterNumbers(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				claims, _ := token.Claims.(jwt.MapClaims)

				rows, _ := h.database.Limit(5).Raw("SELECT DISTINCT account_number, customer_name FROM waters WHERE user_id = ?", claims["userId"]).Rows()
				var waterRes []model.WaterRes
				for rows.Next() {
					var waterRess model.WaterRes
					h.database.ScanRows(rows, &waterRess)
					waterRes = append(waterRes, waterRess)
				}

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				json.NewEncoder(w).Encode(waterRes)

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}

}

func (h Handler) CreateBank(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				var bank model.BankRequest
				defer r.Body.Close()
				err := json.NewDecoder(r.Body).Decode(&bank)
				if err != nil {
					http.Error(w, "Bad Request", http.StatusBadRequest)
					return
				}

				id := uuid.NewV4()

				res := h.database.Create(&model.Bank{BankId: id, BankName: bank.BankName, Paybill: bank.Paybill}).Scan(&model.Bank{})

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				json.NewEncoder(w).Encode(res)

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}
}

func (h Handler) GetBanks(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	authorizationHeader := r.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an err")
				}
				return []byte(settings.GetSecret()), nil
			})
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
				return
			}
			if token.Valid {

				rows, err := h.database.Model(&model.Bank{}).Rows()
				if err != nil {
					fmt.Println(err)
				}

				var banks []model.Bank
				for rows.Next() {
					var bank model.Bank
					h.database.ScanRows(rows, &bank)
					banks = append(banks, bank)
				}

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				json.NewEncoder(w).Encode(banks)

			} else {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(model.Response{Message: "Invalid Authorization Token"})
			}
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.Response{Message: "An Authorization Header is Required"})
	}
}
