package model

import (
	"github.com/satori/go.uuid"
	"time"
)

type Mwangaza struct {
	CreatedAt time.Time `json:"created_at"`
	CreatedBy string    `gorm:"type:varchar(150);"  json:"created_by"`
	Active    bool      `gorm:"default:true" json:"active"`
}

type Response struct {
	Message string `json:"message"`
}

type TransactionResponse struct {
	Message  string    `json:"message"`
	TransRef uuid.UUID `json:"trans_ref"`
}

type Transaction struct {
	TransactionId uuid.UUID `gorm:"type:uuid; primary_key" json:"transaction_id"`
	UserId        uuid.UUID `gorm:"type:uuid" json:"user_id"`
	Name          string    `gorm:"type:varchar(150); not null" json:"name"`
	Amount        int       `json:"amount"`
	Mwangaza
}

type ShopTransaction struct {
	TransactionId uuid.UUID `gorm:"type:uuid; primary_key" json:"transaction_id"`
	ShopId        uuid.UUID `gorm:"type:uuid" json:"shop_id"`
	Name          string    `gorm:"type:varchar(150); not null" json:"name"`
	Amount        int       `json:"amount"`
	Commission    int       `json:"commission"`
	Mwangaza
}

type TransactionRes struct {
	TransactionId uuid.UUID `gorm:"type:uuid; primary_key" json:"transaction_id"`
	Name          string    `gorm:"type:varchar(150); not null" json:"name"`
	Amount        int       `json:"amount"`
}

type PhoneRes struct {
	PhoneNumber string `json:"phone_number"`
}

type PrePaidRes struct {
	MeterNumber string `json:"meter_number"`
}

type PostPaidRes struct {
	AccountNumber string `json:"account_number"`
}

type WaterRes struct {
	AccountNumber string `json:"account_number"`
	CustomerName  string `json:"customer_name"`
}

type User struct {
	UserId        uuid.UUID `gorm:"type:uuid; primary_key" json:"user_id"`
	Name          string    `gorm:"type:varchar(150)" json:"name"`
	PhoneNumber   string    `gorm:"type:varchar(15); unique; not null" json:"phone_number"`
	Pin           string    `gorm:"not null" json:"pin"`
	Shop          bool      `gorm:"default:false" json:"active"`
	DeviceId      string    `gorm:"type:varchar(510)" json:"device_id"`
	FacebookToken string    `gorm:"type:varchar(510)" json:"facebook_token"`
	Mwangaza
}

type Shop struct {
	ShopId        uuid.UUID `gorm:"type:uuid; primary_key" json:"shop_id"`
	ShopName      string    `gorm:"type:varchar(150); not null" json:"shop_name"`
	PhoneNumber   string    `gorm:"type:varchar(15); unique; not null" json:"phone_number"`
	Pin           string    `gorm:"not null"json:"pin"`
	FacebookToken string    `gorm:"type:varchar(510);" json:"facebook_token"`
}

type Business struct {
	BusinessId          uuid.UUID `gorm:"type:uuid; primary_key" json:"business_id"`
	FullNames           string    `gorm:"type:varchar(150); not null" json:"full_names"`
	BusinessAddress     string    `gorm:"type:varchar(150); not null" json:"business_address"`
	BusinessLocation    string    `gorm:"type:varchar(150); not null" json:"business_location"`
	BusinessDescription string    `gorm:"type:varchar(150); not null" json:"business_description"`
	PhoneNumber         string    `gorm:"type:varchar(15); unique; not null" json:"phone_number"`
	Pin                 string    `gorm:"not null"json:"pin"`
	ReferralCode        string    `gorm:"type:varchar(510);" json:"referral_code"`
	Mwangaza
}

type RegisterUser struct {
	Name          string `json:"name"`
	PhoneNumber   string `json:"phone_number"`
	FacebookToken string `json:"facebook_token"`
	Pin           string `json:"pin"`
}

type LoginUser struct {
	PhoneNumber string `json:"phone_number"`
	Pin         string `json:"pin"`
}

type VerifyUser struct {
	PhoneNumber string `json:"phone_number"`
}

type Wallet struct {
	WalletId uuid.UUID `gorm:"type:uuid; primary_key" json:"wallet_id"`
	UserId   uuid.UUID `gorm:"type:uuid;" json:"user_id"`
	Balance  int       `gorm:"default: 0" json:"balance"`
	Mwangaza
}

type ShopWallet struct {
	WalletId   uuid.UUID `gorm:"type:uuid; primary_key" json:"wallet_id"`
	ShopId     uuid.UUID `gorm:"type:uuid;" json:"shop_id"`
	Balance    int       `gorm:"default: 0" json:"balance"`
	Commission int       `gorm:"default: 0" json:"commission"`
	Mwangaza
}

type BusinessWallet struct {
	WalletId   uuid.UUID `gorm:"type:uuid; primary_key" json:"wallet_id"`
	BusinessId uuid.UUID `gorm:"type:uuid;" json:"business_id"`
	Balance    int       `gorm:"default: 0" json:"balance"`
	Mwangaza
}

type SendMoneyTariff struct {
	SendMoneyTariffId uuid.UUID `gorm:"type:uuid; primary_key" json:"send_money_tariff_id"`
	UserId            uuid.UUID `gorm:"type:uuid;" json:"user_id"`
	ShareMoneyId      uuid.UUID `gorm:"type:uuid;" json:"share_money_id"`
	Amount            int       `json:"amount"`
	Mwangaza
}

type PaybillTariff struct {
	PaybillTariffId uuid.UUID `gorm:"type:uuid; primary_key" json:"paybill_tariff_id"`
	UserId          uuid.UUID `gorm:"type:uuid;" json:"user_id"`
	PaybillId       uuid.UUID `gorm:"type:uuid;" json:"paybill_id"`
	Amount          int       `json:"amount"`
	Mwangaza
}

type Account struct {
	UserID  string `json:"user_id"`
	Balance int    `json:"balance"`
}

type ShopWalletBalance struct {
	ShopId     string `json:"shop_id"`
	Balance    int    `json:"balance"`
	Commission int    `json:"commission"`
}

type JwtToken struct {
	Token string `json:"token"`
}

type UserAccount struct {
	UserId        uuid.UUID `json:"user_id"`
	Name          string    `json:"name"`
	PhoneNumber   string    `json:"phone_number"`
	DeviceId      string    `json:"device_id"`
	FacebookToken string    `json:"facebook_token"`
	Active        bool      `json:"active"`
	JwtToken
}

type ShopAccount struct {
	ShopId        uuid.UUID `json:"shop_id"`
	ShopName      string    `json:"shop_name"`
	PhoneNumber   string    `json:"phone_number"`
	DeviceId      string    `json:"device_id"`
	FacebookToken string    `json:"facebook_token"`
	Active        bool      `json:"active"`
	JwtToken
}

type BusinessAccount struct {
	BusinessId          uuid.UUID `json:"business_id"`
	FullNames           string    `json:"full_names"`
	BusinessAddress     string    `json:"business_address"`
	BusinessLocation    string    `json:"business_location"`
	BusinessDescription string    `json:"business_description"`
	PhoneNumber         string    `json:"phone_number"`
	ReferralCode        string    `json:"referral_code"`
	Active              bool      `json:"active"`
	JwtToken
}

type Firebase struct {
	FirebaseToken string `json:"firebase_token"`
}

type UserData struct {
	Name string `json:"name"`
}

type Otp struct {
	OtpId  uuid.UUID `gorm:"type:uuid; primary_key" json:"otp_id"`
	UserId uuid.UUID `gorm:"type:uuid" json:"user_id"`
	Otp    string    `json:"otp"`
	Active bool      `json:"active"`
}

type VerifyOtp struct {
	UserId uuid.UUID `json:"user_id"`
	Otp    string    `json:"otp"`
}

type RequestOtp struct {
	PhoneNumber string `json:"phone_number"`
}

type UpdateOtp struct {
	UserId uuid.UUID `json:"user_id"`
	Pin    string    `json:"pin"`
}

type UserReset struct {
	UserId      uuid.UUID `json:"user_id"`
	PhoneNumber string    `json:"phone_number"`
}

type MpesaTransaction struct {
	MpesaTransactionId uuid.UUID `gorm:"type:uuid; primary_key" json:"mpesa_transaction_id"`
	TransID            string    `gorm:"type:varchar(150)" json:"trans_id"`
	TransTime          string    `gorm:"type:varchar(150)" json:"trans_time"`
	TransAmount        string    `gorm:"type:varchar(150)" json:"trans_amount"`
	BusinessShortCode  string    `gorm:"type:varchar(150)" json:"business_short_code"`
	OrgAccountBalance  string    `gorm:"type:varchar(150)" json:"org_account_balance"`
	MSISDN             string    `gorm:"type:varchar(150)" json:"msisdn"`
	FirstName          string    `gorm:"type:varchar(150)" json:"first_name"`
	MiddleName         string    `gorm:"type:varchar(150)" json:"middle_name"`
	LastName           string    `gorm:"type:varchar(150)" json:"last_name"`
	Mwangaza
}

type MpesaPull struct {
	MpesaPullId   uuid.UUID `gorm:"type:uuid; primary_key" json:"mpesa_pull_id"`
	TransID       string    `gorm:"type:varchar(150); unique" json:"trans_id"`
	BillReference string    `gorm:"type:varchar(150)" json:"bill_reference"`
	TransTime     string    `gorm:"type:varchar(150)" json:"trans_time"`
	TransAmount   string    `gorm:"type:varchar(150)" json:"trans_amount"`
	MSISDN        string    `gorm:"type:varchar(150)" json:"msisdn"`
	Mwangaza
}

type MpesaReconcile struct {
	MpesaReconcileId uuid.UUID `gorm:"type:uuid; primary_key" json:"mpesa_reconcile_id"`
	TransID          string    `gorm:"type:varchar(150); unique" json:"trans_id"`
	BillReference    string    `gorm:"type:varchar(150)" json:"bill_reference"`
	TransTime        string    `gorm:"type:varchar(150)" json:"trans_time"`
	TransAmount      string    `gorm:"type:varchar(150)" json:"trans_amount"`
	MSISDN           string    `gorm:"type:varchar(150)" json:"msisdn"`
	Mwangaza
}

type WalletDeposits struct {
	MpesaTransactionId uuid.UUID `json:"mpesa_transaction_id"`
	TransID            string    `json:"trans_id"`
	TransTime          string    `json:"trans_time"`
	TransAmount        string    `json:"trans_amount"`
}

type ShareFunds struct {
	PhoneNumber string `json:"phone_number"`
	Amount      uint   `json:"amount"`
}

type ShopMoveFunds struct {
	Amount int `json:"amount"`
}

type ShareFundsRes struct {
	Name        string `json:"name"`
	PhoneNumber string `json:"phone_number"`
	Amount      int    `json:"amount"`
	Balance     int    `json:"balance"`
}

type C2BPull struct {
	ResponseRefID   string `json:"ResponseRefID"`
	ResponseCode    string `json:"ResponseCode"`
	ResponseMessage string `json:"ResponseMessage"`
	Response        [][]struct {
		TransactionID    string `json:"transactionId"`
		TrxDate          string `json:"trxDate"`
		Msisdn           string `json:"msisdn"`
		Sender           string `json:"sender"`
		Transactiontype  string `json:"transactiontype"`
		Billreference    string `json:"billreference"`
		Amount           int    `json:"amount"`
		Organizationname string `json:"organizationname"`
	} `json:"Response"`
}

type C2BCallBack struct {
	TransactionType   string `json:"TransactionType"`
	TransID           string `json:"TransID"`
	TransTime         string `json:"TransTime"`
	TransAmount       string `json:"TransAmount"`
	BusinessShortCode string `json:"BusinessShortCode"`
	BillRefNumber     string `json:"BillRefNumber"`
	InvoiceNumber     string `json:"InvoiceNumber"`
	OrgAccountBalance string `json:"OrgAccountBalance"`
	ThirdPartyTransID string `json:"ThirdPartyTransID"`
	MSISDN            string `json:"MSISDN"`
	FirstName         string `json:"FirstName"`
	MiddleName        string `json:"MiddleName"`
	LastName          string `json:"LastName"`
}

type Water struct {
	WaterId       uuid.UUID `gorm:"type:uuid; primary_key" json:"water_id"`
	UserId        uuid.UUID `gorm:"type:uuid; primary_key" json:"user_id"`
	AccountNumber string    `gorm:"type:varchar(150)" json:"account_number"`
	CustomerName  string    `gorm:"type:varchar(150)" json:"customer_name"`
	DueDate       time.Time `gorm:"type:varchar(150)" json:"due_date"`
	Amount        int       `gorm:"type:varchar(150)" json:"amount"`
	Paid          string    `gorm:"type:varchar(150); default: 0" json:"paid"`
	PaymentTypeId string    `gorm:"type:varchar(150)" json:"payment_type_id"`
	TransactionId string    `gorm:"type:varchar(150)" json:"transaction_id"`
	ServiceNumber string    `gorm:"type:varchar(150)" json:"service_number"`
	Mwangaza
}

type WaterRequest struct {
	UserId        uuid.UUID `json:"user_id"`
	AccountNumber string    `json:"account_number"`
}

type WaterPresentation struct {
	WaterId uuid.UUID `json:"water_id"`
	Amount  uint      `json:"amount"`
}

type WaterResponse struct {
	WaterId       uuid.UUID `json:"water_id"`
	UserId        uuid.UUID `json:"user_id"`
	AccountNumber string    `json:"account_number"`
	CustomerName  string    `json:"customer_name"`
	DueDate       time.Time `json:"due_date"`
	Amount        int       `json:"amount"`
}

type WaterBill struct {
	WaterId       uuid.UUID `json:"water_id"`
	AccountNumber string    `json:"account_number"`
	Amount        string    `json:"amount"`
	Paid          string    `json:"paid"`
}

type Airtime struct {
	AirtimeId         uuid.UUID `gorm:"type:uuid; primary_key" json:"airtime_id"`
	UserId            uuid.UUID `gorm:"type:uuid; primary_key" json:"user_id"`
	PhoneNumber       string    `gorm:"type:varchar(150)" json:"phone_number"`
	Amount            int       `gorm:"type:varchar(150)" json:"amount"`
	Operator          string    `gorm:"type:varchar(150)" json:"operator"`
	ReceiptNumber     string    `gorm:"type:varchar(150)" json:"receipt_number"`
	Date              string    `gorm:"type:varchar(150)" json:"date"`
	TransactionId     int       `gorm:"type:varchar(150)" json:"transaction_id"`
	TransactionStatus bool      `gorm:"default:false" json:"transaction_status"`
	StatusCode        int       `gorm:"type:varchar(150)" json:"status_code"`
	StatusMessage     string    `gorm:"type:varchar(150)" json:"status_message"`
	Mwangaza
}

type ShopAirtime struct {
	AirtimeId         uuid.UUID `gorm:"type:uuid; primary_key" json:"airtime_id"`
	ShopId            uuid.UUID `gorm:"type:uuid; primary_key" json:"shop_id"`
	PhoneNumber       string    `gorm:"type:varchar(150)" json:"phone_number"`
	Amount            int       `gorm:"type:varchar(150)" json:"amount"`
	Operator          string    `gorm:"type:varchar(150)" json:"operator"`
	ReceiptNumber     string    `gorm:"type:varchar(150)" json:"receipt_number"`
	Date              string    `gorm:"type:varchar(150)" json:"date"`
	TransactionId     int       `gorm:"type:varchar(150)" json:"transaction_id"`
	TransactionStatus bool      `gorm:"default:false" json:"transaction_status"`
	StatusCode        int       `gorm:"type:varchar(150)" json:"status_code"`
	StatusMessage     string    `gorm:"type:varchar(150)" json:"status_message"`
	Mwangaza
}

type AirtimeVoucher struct {
	AirtimeId         uuid.UUID `gorm:"type:uuid; primary_key" json:"airtime_id"`
	UserId            uuid.UUID `gorm:"type:uuid; primary_key" json:"user_id"`
	Ref               string    `gorm:"type:varchar(150)" json:"ref"`
	Amount            int       `gorm:"type:varchar(150)" json:"amount"`
	Operator          string    `gorm:"type:varchar(150)" json:"operator"`
	Pincode           string    `gorm:"type:varchar(150)" json:"pincode"`
	Date              string    `gorm:"type:varchar(150)" json:"date"`
	TransactionId     int       `gorm:"type:varchar(150)" json:"transaction_id"`
	TransactionStatus bool      `gorm:"default:false" json:"transaction_status"`
	StatusCode        int       `gorm:"type:varchar(150)" json:"status_code"`
	StatusMessage     string    `gorm:"type:varchar(150)" json:"status_message"`
	Mwangaza
}

type ShopAirtimeVoucher struct {
	AirtimeId         uuid.UUID `gorm:"type:uuid; primary_key" json:"airtime_id"`
	ShopId            uuid.UUID `gorm:"type:uuid; primary_key" json:"shop_id"`
	Ref               string    `gorm:"type:varchar(150)" json:"ref"`
	Amount            int       `gorm:"type:varchar(150)" json:"amount"`
	Operator          string    `gorm:"type:varchar(150)" json:"operator"`
	Pincode           string    `gorm:"type:varchar(150)" json:"pincode"`
	Date              string    `gorm:"type:varchar(150)" json:"date"`
	TransactionId     int       `gorm:"type:varchar(150)" json:"transaction_id"`
	TransactionStatus bool      `gorm:"default:false" json:"transaction_status"`
	StatusCode        int       `gorm:"type:varchar(150)" json:"status_code"`
	StatusMessage     string    `gorm:"type:varchar(150)" json:"status_message"`
	Mwangaza
}

type AirtimeRequest struct {
	PhoneNumber string `json:"phone_number"`
	Amount      uint   `json:"amount"`
	Operator    string `json:"operator"`
}

type AirtimeVoucherRequest struct {
	Amount   uint   `json:"amount"`
	Operator string `json:"operator"`
}

type AirtimeResponse struct {
	AirtimeId         uuid.UUID `json:"airtime_id"`
	UserId            uuid.UUID `json:"user_id"`
	PhoneNumber       string    `json:"phone_number"`
	Amount            int       `json:"amount"`
	ReceiptNumber     string    `json:"receipt_number"`
	TransactionStatus bool      `json:"transaction_status"`
}

type ShopAirtimeResponse struct {
	AirtimeId         uuid.UUID `json:"airtime_id"`
	ShopId            uuid.UUID `json:"shop_id"`
	PhoneNumber       string    `json:"phone_number"`
	Amount            int       `json:"amount"`
	ReceiptNumber     string    `json:"receipt_number"`
	TransactionStatus bool      `json:"transaction_status"`
}

type ShopAirtimeVoucherResponse struct {
	AirtimeId         uuid.UUID `json:"airtime_id"`
	ShopId            uuid.UUID `json:"shop_id"`
	Ref               string    `json:"ref"`
	Amount            int       `json:"amount"`
	Pincode           string    `json:"pincode"`
	TransactionStatus bool      `json:"transaction_status"`
}

type PrePaid struct {
	PrePaidId         uuid.UUID `gorm:"type:uuid; primary_key" json:"pre_paid_id"`
	UserId            uuid.UUID `gorm:"type:uuid; primary_key" json:"user_id"`
	MeterNumber       string    `gorm:"type:varchar(150)" json:"meter_number"`
	CustomerName      string    `gorm:"type:varchar(150)" json:"customer_name"`
	TransactionStatus bool      `gorm:"default:false" json:"transaction_status"`
	Units             string    `gorm:"type:varchar(150)" json:"units"`
	ResCode           string    `gorm:"type:varchar(150)" json:"res_code"`
	StdTokenTax       string    `gorm:"type:varchar(150);" json:"std_token_tax"`
	StdTokenAmt       string    `gorm:"type:varchar(150)" json:"std_token_amt"`
	Token             string    `gorm:"type:varchar(150)" json:"token"`
	StdTokenRctNum    string    `gorm:"type:varchar(150)" json:"std_token_rct_num"`
	Date              string    `gorm:"type:varchar(150)" json:"date"`
	TotalAmount       string    `gorm:"type:varchar(150)" json:"total_amount"`
	Ref               string    `gorm:"type:varchar(150)" json:"ref"`
	StatusCode        int       `gorm:"type:varchar(150)" json:"status_code"`
	StatusMessage     string    `gorm:"type:varchar(150)" json:"status_message"`
	Mwangaza
}

type PrePaidRequest struct {
	UserId      uuid.UUID `json:"user_id"`
	MeterNumber string    `json:"meter_number"`
}

type BankRequest struct {
	BankName string `json:"bank_name"`
	Paybill  string `json:"paybill"`
}

type PrePaidResponse struct {
	PrePaidId    uuid.UUID `json:"pre_paid_id"`
	UserId       uuid.UUID `json:"user_id"`
	MeterNumber  string    `json:"meter_number"`
	CustomerName string    `json:"customer_name"`
}

type PrePaidVend struct {
	PrePaidId    uuid.UUID `json:"pre_paid_id"`
	MeterNumber  string    `json:"meter_number"`
	CustomerName string    `json:"customer_name"`
	Amount       uint      `json:"amount"`
}

type PrePaidVendResponse struct {
	PrePaidId    uuid.UUID `json:"pre_paid_id"`
	UserId       uuid.UUID `json:"user_id"`
	MeterNumber  string    `json:"meter_number"`
	CustomerName string    `json:"customer_name"`
	Units        string    `json:"units"`
	TotalAmount  string    `json:"total_amount"`
	Token        string    `json:"token"`
}

type PostPaidRequest struct {
	UserId        uuid.UUID `json:"user_id"`
	AccountNumber string    `json:"account_number"`
}

type PostPaid struct {
	PostPaidId        uuid.UUID `gorm:"type:uuid; primary_key" json:"post_paid_id"`
	UserId            uuid.UUID `gorm:"type:uuid;" json:"user_id"`
	AccountNumber     string    `gorm:"type:varchar(150)" json:"account_number"`
	Amount            string    `gorm:"type:varchar(150)" json:"amount"`
	CustomerName      string    `gorm:"type:varchar(150)" json:"customer_name"`
	CustomerBalance   string    `gorm:"type:varchar(150)" json:"customer_balance"`
	TransactionStatus bool      `gorm:"default:false" json:"transaction_status"`
	RctNum            string    `gorm:"type:varchar(150)" json:"rct_num"`
	Date              string    `gorm:"type:varchar(150)" json:"date"`
	TransId           int       `gorm:"type:varchar(150)" json:"trans_id"`
	Ref               string    `gorm:"type:varchar(150)" json:"ref"`
	StatusCode        int       `gorm:"type:varchar(150)" json:"status_code"`
	StatusMessage     string    `gorm:"type:varchar(150)" json:"status_message"`
	Mwangaza
}

type PostPaidResponse struct {
	PostPaidId      uuid.UUID `json:"post_paid_id"`
	UserId          uuid.UUID `json:"user_id"`
	AccountNumber   string    `json:"account_number"`
	CustomerName    string    `json:"customer_name"`
	CustomerBalance string    `json:"customer_balance"`
}

type PostPaidVend struct {
	PostPaidId    uuid.UUID `json:"post_paid_id"`
	AccountNumber string    `json:"account_number"`
	CustomerName  string    `json:"customer_name"`
	Amount        uint      `json:"amount"`
}

type PostPaidVendResponse struct {
	PostPaidId    uuid.UUID `json:"post_paid_id"`
	UserId        uuid.UUID `json:"user_id"`
	AccountNumber string    `json:"account_number"`
	CustomerName  string    `json:"customer_name"`
	RctNum        string    `json:"rct_num"`
	Amount        string    `json:"amount"`
	TransId       int       `json:"trans_id"`
	Ref           string    `json:"ref"`
}

type Payment struct {
	UserId        uuid.UUID `json:"user_id"`
	Amount        uint      `json:"amount"`
	AccountNumber string    `json:"account_number"`
}

type Utility struct {
	UtilityId         uuid.UUID `gorm:"type:uuid; primary_key" json:"utility_id"`
	UserId            uuid.UUID `gorm:"type:uuid;" json:"user_id"`
	Account           string    `gorm:"type:varchar(80)" json:"account"`
	Amount            int       `gorm:"type:varchar(80)" json:"amount"`
	BillerName        string    `gorm:"type:varchar(80)" json:"biller_name"`
	MerchantReference string    `gorm:"type:varchar(80)" json:"merchant_reference"`
	Phone             string    `gorm:"type:varchar(80)" json:"phone"`
	Status            bool      `json:"status"`
	Mwangaza
}

type UtilitiesRequest struct {
	Account           string `json:"account"`
	Amount            uint   `json:"amount"`
	BillerName        string `json:"biller_name"`
	MerchantReference string `json:"merchant_reference"`
	Phone             string `json:"phone"`
}

type Paybill struct {
	UserId        uuid.UUID `json:"user_id"`
	Amount        uint      `json:"amount"`
	Paybill       string    `json:"paybill"`
	AccountNumber string    `json:"account_number"`
}

type MpesaB2cPayments struct {
	BusinessCustomerPaymentId        uuid.UUID `gorm:"type:uuid; primary_key" json:"business_customer_payment_id"`
	UserId                           uuid.UUID `gorm:"type:uuid; not null" json:"user_id"`
	Amount                           string    `gorm:"type:varchar(20)" json:"amount"`
	AccountNumber                    string    `gorm:"type:varchar(20)" json:"account_number"`
	ConversationId                   string    `gorm:"type:varchar(60)" json:"conversation_id"`
	OriginatorConversationId         string    `gorm:"type:varchar(60)" json:"originator_conversation_id"`
	TransactionReceipt               string    `gorm:"type:varchar(20)" json:"transaction_receipt"`
	ReceiverPartyPublicName          string    `gorm:"type:varchar(80)" json:"receiver_party_public_name"`
	B2cRecipientIsRegisteredCustomer string    `gorm:"type:varchar(20)" json:"b2c_recipient_is_registered_customer"`
	TransactionCompletedDatetime     time.Time `json:"transaction_completed_datetime"`
	Metadata                         string    `json:"metadata"`
	TransactionStatus                bool      `gorm:"default:false" json:"transaction_status"`
	Mwangaza
}

// MpesaB2bPayments is MpesaB2bPayments
type MpesaB2bPayments struct {
	BusinessBusinessPaymentId    uuid.UUID `gorm:"type:uuid; primary_key" json:"business_business_payment_id"`
	UserId                       uuid.UUID `gorm:"type:uuid; not null" json:"user_id"`
	Amount                       string    `gorm:"type:varchar(20)" json:"amount"`
	AccountNumber                string    `gorm:"type:varchar(60)" json:"account_number"`
	PayBill                      string    `gorm:"type:varchar(60)" json:"pay_bill"`
	ConversationId               string    `gorm:"type:varchar(60)" json:"conversation_id"`
	OriginatorConversationId     string    `gorm:"type:varchar(60)" json:"originator_conversation_id"`
	TransactionReceipt           string    `gorm:"type:varchar(60)" json:"transaction_receipt"`
	OrganizationShortCode        string    `gorm:"type:varchar(80)" json:"organization_short_code"`
	BankName                     string    `gorm:"type:varchar(80)" json:"bank_name"`
	TransactionCompletedDatetime time.Time `json:"transaction_completed_datetime"`
	Metadata                     string    `json:"metadata"`
	TransactionStatus            bool      `gorm:"default:false" json:"transaction_status"`
	Mwangaza
}

// MPESAResponse is MPESAResponse
type MPESAResponse struct {
	ConversationID           string `json:"ConversationID"`
	OriginatorConversationID string `json:"OriginatorConversationID"`
	ResponseCode             string `json:"ResponseCode"`
	ResponseDescription      string `json:"ResponseDescription"`
}

// B2CCallBack is B2CCallBack
type B2CCallBack struct {
	Result struct {
		ResultType               int    `json:"ResultType"`
		ResultCode               int    `json:"ResultCode"`
		ResultDesc               string `json:"ResultDesc"`
		OriginatorConversationID string `json:"OriginatorConversationID"`
		ConversationID           string `json:"ConversationID"`
		TransactionID            string `json:"TransactionID"`
		ResultParameters         struct {
			ResultParameter []struct {
				Key   string      `json:"Key"`
				Value interface{} `json:"Value"`
			} `json:"ResultParameter"`
		} `json:"ResultParameters"`
		ReferenceData struct {
			ReferenceItem struct {
				Key   string `json:"Key"`
				Value string `json:"Value"`
			} `json:"ReferenceItem"`
		} `json:"ReferenceData"`
	} `json:"Result"`
}

// B2BCallBack is B2BCallBack
type B2BCallBack struct {
	Result struct {
		ResultType               int    `json:"ResultType"`
		ResultCode               int    `json:"ResultCode"`
		ResultDesc               string `json:"ResultDesc"`
		OriginatorConversationID string `json:"OriginatorConversationID"`
		ConversationID           string `json:"ConversationID"`
		TransactionID            string `json:"TransactionID"`
		ResultParameters         struct {
			ResultParameter []struct {
				Key   string      `json:"Key"`
				Value interface{} `json:"Value,omitempty"`
			} `json:"ResultParameter"`
		} `json:"ResultParameters"`
		ReferenceData struct {
			ReferenceItem []struct {
				Key   string      `json:"Key"`
				Value interface{} `json:"Value,omitempty"`
			} `json:"ReferenceItem"`
		} `json:"ReferenceData"`
	} `json:"Result"`
}

type Bank struct {
	BankId   uuid.UUID `gorm:"type:uuid; primary_key" json:"bank_id"`
	BankName string    `gorm:"type:varchar(150); not null" json:"bank_name"`
	Paybill  string    `gorm:"type:varchar(50); unique; not null" json:"paybill"`
	Mwangaza
}
